import sys
import argparse
from datetime import datetime

from credentials import db_alias_to_credentials_map
from upload_db_table import run_insertion

def __validate_input():
  parser = argparse.ArgumentParser(description='Loads uzio csv/excel data to eligibility_load_history database.')
  parser.add_argument('--source-file', dest='source_file', required=True, help=f'Source csv/excel file path')
  parser.add_argument('--target-db', dest='target_db_alias', required=False, choices=db_alias_to_credentials_map.keys(), help=f'Target db alias name', default='PROD')
  parser.add_argument('--csv-delimiter', dest='csv_delimiter', required=False, help=f'Csv delimiter used in source file (defaults to comma; does not apply to excel file)', default=',')
  parser.add_argument('--read-record-buffer', dest='read_record_buffer', type=int, required=False, help=f'Number of rows to read from the csv file in each iteration (defaults to all rows altogether)', default=0)
  parser.add_argument('--max-record-count', dest='max_record_count', type=int, required=False, help=f'Maxmimum number of rows insert in db (defaults to all rows)', default=0)
  parser.add_argument('--source-date-format', dest='source_date_format', required=False, help=f'Source date format to parse', default='MM/DD/YYYY')
  parser.add_argument('--insert-batch-size', dest='insert_batch_size', type=int, required=False, help=f'Number of records to upload and insert to db in each iteration (defaults to 1000)', default=1000)

  try:
    args = parser.parse_args()
    return (args.source_file,
            args.target_db_alias,
            args.csv_delimiter,
            args.read_record_buffer,
            args.max_record_count,
            args.source_date_format.replace('YYYY', '%Y').replace('MM', '%m').replace('DD', '%d'),
            args.insert_batch_size)
  except:
    parser.print_help()
    sys.exit(0)

__history_table_name = 'eligibility_load_history'
__history_data_excel_to_db_field_names_map = {
  'RECORD ID': 'record_id',
  'SUBCRIBER SSN': 'subscriber_ssn',
  'GROUP NUMBER': 'group_number',
  'LAST NAME': 'last_name',
  'FIRST NAME': 'first_name',
  'MIDDLE NAME': 'middle_name',
  'ADDRESS': 'address',
  'ADDRESS LINE 2': 'address_line_2',
  'CITY': 'city',
  'COUNTY': 'county',
  'STATE': 'state',
  'ZIP CODE': 'zip_code',
  'PHONE': 'phone',
  'GENDER': 'gender',
  'RELATIONSHIP CODE': 'relationship_code',
  'RELATIONSHIP TO EMPLOYEE': 'relation_to_employee',
  'BIRTH DATE': 'birth_date',
  'BENEFIT PLAN CODE': 'benefit_plan_code',
  'BENEFIT PLAN NAME': 'benefit_plan_name',
  'ENROLLMENT STATUS': 'enrollment_status',
  'ENROLLMENT DATE': 'enrollment_date',
  'ELIGIBILITY START DATE': 'eligibility_start_date',
  'ELIGIBILITY END DATE': 'eligibility_end_date',
  'DISENROLL DATE': 'disenroll_date',
  'PLAN TYPE': 'plan_type',
  'PCP': 'pcp',
  'PCP BEGIN DATE': 'pcp_begin_date',
  'PCP END DATE': 'pcp_end_date',
  'REGION': 'region',
  'OTHER INSURANCE': 'other_insurance',
  'INSURANCE': 'insurance',
  'INSURANCE BEGIN DATE': 'insurance_begin_date',
  'INSURANCE END DATE': 'insurance_end_date',
  'DISENROLL CODE': 'disenroll_code',
  'MEDICARE NUMBER': 'medicare_number',
  'EMPLOYMENT DATE': 'employment_date',
  'MEMBER EMAIL ADDRESS': 'member_email_address',
  'EMPLOYEE ID': 'employee_id',
  'PREVIOUS SUBSCRIBER SSN': 'previous_subscriber_ssn',
  'PREVIOUS LAST NAME': 'previous_last_name',
  'PREVIOUS FIRST NAME': 'previous_first_name',
  'PREVIOUS GENDER': 'previous_gender',
  'PREVIOUS RELATIONSHIP CODE': 'previous_relationship_code',
  'PREVIOUS BIRTHDATE': 'previous_birth_date',
  'CHANGE EFFECTIVE DATE': 'change_effective_date',
  'CHANGE CODE': 'change_code',
  'CHANGE REASON': 'change_reason',
  'COBRA QUALIFYING EVENT DATE (BEGIN DATE)': 'cobra_qualifying_event_date',
  'COBRA TERMINATION DATE': 'cobra_termination_date',
  'COBRA QUALIFYING EVENT': 'cobra_qualifying_event',
  'DEPENDENT LAST NAME': 'dependent_last_name',
  'DEPENDENT FIRST NAME': 'dependent_first_name',
  'DEPENDENT MIDDLE NAME': 'dependent_middle_name',
  'DEPENDENT SSN': 'dependent_ssn',
  'DEPENDENT DATE OF BIRTH': 'dependent_birth_date',
  'SPOUSE LAST NAME': 'spouse_last_name',
  'SPOUSE FIRST NAME': 'spouse_first_name',
  'SPOUSE MIDDLE NAME': 'spouse_middle_name',
  'SPOUSE SSN': 'spouse_ssn',
  'SPOUSE DATE OF BIRTH': 'spouse_birth_date',
  'MEMBER TYPE': 'member_type'
}
__history_data_default_values = {
  'group_number': '8000',
  'enrollment_date': datetime(2019, 3, 1),
  'eligibility_end_date': datetime(2099, 3, 1),
  'region': 'DEFAULT'
}
__history_batch = {
  'batch_id': None,
  'batch_uploaded_date': None
}

def __refine_history_data(data_dict_list, db_loader):
  for item in data_dict_list:
    item['batch_id'] = __history_batch['batch_id']
    item['batch_uploaded_date'] = __history_batch['batch_uploaded_date']
    if item['benefit_plan_name'] and item['benefit_plan_name'][0].isdigit():
      item['benefit_plan_code'], item['benefit_plan_name'] = item['benefit_plan_name'], item['benefit_plan_code']
    for field_name in __history_data_default_values:
      if field_name in item and not item[field_name]:
        item[field_name] = __history_data_default_values[field_name]

def __set_batch_number(db_loader):
  max_batch_id = db_loader.fetch_all_from_query(f'SELECT MAX(batch_id) as max_batch_size FROM {__history_table_name}')
  __history_batch['batch_id'] = str(int(max_batch_id[0]['max_batch_size'] or 0) + 1).zfill(5)
  __history_batch['batch_uploaded_date'] = datetime.utcnow()
  print(f'Uploading batch "{__history_batch["batch_id"]}" to table "{__history_table_name}" at {__history_batch["batch_uploaded_date"].strftime("%m/%d/%Y %H:%M:%S")} UTC...')
  print('-' * 80)

if __name__ == '__main__':
  source_file, target_db_alias, csv_delimiter, read_record_buffer, max_record_count, source_date_format, insert_batch_size = __validate_input()
  run_insertion(source_file=source_file, csv_delimiter=csv_delimiter, read_record_buffer=read_record_buffer, max_record_count=max_record_count, source_date_format=source_date_format, insert_batch_size=insert_batch_size, target_db_alias=target_db_alias, target_table_name=__history_table_name, resume_insertion=False, refine_data_function=__refine_history_data, field_names_map=__history_data_excel_to_db_field_names_map, pre_insert_function=__set_batch_number)
