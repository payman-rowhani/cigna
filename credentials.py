import sys

class MySqlAliasError(Exception):
  pass

class FtpCredentialsFormatError(Exception):
  pass

# Benzer PROD mysql db credentials
prod_db_endpoint = '174.129.112.77'
prod_db_username = 'webuser'
prod_db_password = 'webuser'
prod_db_name = 'tpa'
prod_db_alias = 'PROD'

# RDS Benebay mysql db credentials
rds_db_endpoint = 'bnby-tpa-dev-01.cnlwxbwkx7dj.us-east-2.rds.amazonaws.com'
rds_db_username = 'read_only'
rds_db_password = 'benebay_read_only'
rds_db_name = 'tpa'
rds_db_alias = 'RDS'

# TPA Benebay mysql db credentials
tpa_db_endpoint = 'tpa.inmedicas.com'
tpa_db_username = 'mark'
tpa_db_password = '352689FW00KMbmN'
tpa_db_name = 'tpa'
tpa_db_alias = 'TPA'

# PAYMAN EC2 mysql db credentials
payman_db_endpoint = 'ec2-35-170-191-97.compute-1.amazonaws.com'
payman_db_username = 'admin'
payman_db_password = 'admin'
payman_db_name = 'initial'
payman_db_alias = 'PAYMAN'

# db alias to credentials map
db_alias_to_credentials_map = {
  prod_db_alias: {
    'db_endpoint': prod_db_endpoint,
    'db_username': prod_db_username,
    'db_password': prod_db_password,
    'db_name': prod_db_name,
  },
  rds_db_alias: {
    'db_endpoint': rds_db_endpoint,
    'db_username': rds_db_username,
    'db_password': rds_db_password,
    'db_name': rds_db_name,
  },
  tpa_db_alias: {
    'db_endpoint': tpa_db_endpoint,
    'db_username': tpa_db_username,
    'db_password': tpa_db_password,
    'db_name': tpa_db_name,
  },
  payman_db_alias: {
    'db_endpoint': payman_db_endpoint,
    'db_username': payman_db_username,
    'db_password': payman_db_password,
    'db_name': payman_db_name,
  }
}

# test aws credentials
test_aws_region_name = 'us-east-1'
test_aws_access_key_id = 'AKIAICBIO225NQJACS6Q'
test_aws_secret_access_key = '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'

# production aws credentials
aws_region_name = 'us-east-1'
aws_access_key_id = 'AKIAIFDFHHMZHJFIUCQQ'
aws_secret_access_key = '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'

# sftp server credentials
sftp_endpoint = None
sftp_username = None
sftp_password = None
sftp_private_key = None
sftp_private_key_pass = None

# local output directory path
local_output_directory = '/home/lrivers'

# vendor to bucket name map
vendor_to_bucket_map = {
  'carevalet': 'benzer.benebay-carevalet',
  'cigna': 'benzer.benebay-cigna',
  'sgrx': 'benzer.benebay-sgrx',
  'uzio': 'benzer.benebay.uzio'
}

# sftp credentials bucket name
ftp_credentials_bucket_name = 'benzer.benebay.ftp-deliveries'

# vendor to sftp credential map
vendor_to_ftp_credentials_map = {
  'cigna': 'ftp-credentials-cigna.txt',
  'sgrx': 'ftp-credentials-sgrx.txt',
  'carevalet': 'ftp-credentials-carevalet.txt',
  'uzio': 'ftp-credentials-uzio.txt'
}

# vendor to sftp claims credential map
vendor_to_ftp_claims_credentials_map = {
  'cigna': 'ftp-credentials-cigna-claims.txt',
  'sgrx': 'ftp-credentials-sgrx-claims.txt',
  # 'carevalet': 'ftp-credentials-carevalet-claims.txt',
  # 'uzio': 'ftp-credentials-uzio-claims.txt'
}

# pgp info
pgp_s3_private_key_bucket = vendor_to_bucket_map['cigna']
pgp_s3_private_key_file_path = 'claims/pgpkeys/0x56B88AB2-sec.asc'
pgp_passphrase = 'benebay'
pgp_encrypted_file_extension = '.pgp'

# get aws utils
def get_aws_utils(use_test=False):
  from common.send.aws_utils import AwsUtils

  if not use_test:
    return AwsUtils(aws_region_name, aws_access_key_id, aws_secret_access_key)
  else:
    return AwsUtils(test_aws_region_name, test_aws_access_key_id, test_aws_secret_access_key)

# db loader by alias
def get_db_loader(db_alias, connect_timeout=30, use_single_connection=True, show_output=True, exit_on_failed=True):
  from common.data.mysql_utils import MySqlUtils

  db_alias = db_alias.upper()
  db_cred = db_alias_to_credentials_map.get(db_alias.upper())
  if not db_cred:
    if show_output:
      print(f'Error: Invalid db alias \'{db_alias}\' passed! Please use one of the following: {", ".join(db_alias_to_credentials_map.keys())}')
    if exit_on_failed:
      sys.exit(0)
    else:
      raise MySqlAliasError()
  db_loader = MySqlUtils(db_cred['db_endpoint'], db_cred['db_username'], db_cred['db_password'], db_cred['db_name'], connect_timeout=connect_timeout, use_single_connection=use_single_connection)
  return db_loader

# get sftp credentials and utils by vendor
def get_ftp_credentials(vendor, is_claims_credential=False, sftp_port=22, show_output=True, exit_on_failed=True):
  from common.send.sftp_utils import SftpUtils

  aws_utils = get_aws_utils()

  vendor = vendor.lower()
  credential_file_name = vendor_to_ftp_claims_credentials_map[vendor] if is_claims_credential else vendor_to_ftp_credentials_map[vendor]

  if not aws_utils.s3_file_exists(ftp_credentials_bucket_name, credential_file_name):
    if show_output:
      print(f'Could not find the credentials file for vendor "{vendor}" in bucket "{ftp_credentials_bucket_name}"')
    if exit_on_failed:
      sys.exit(0)
    else:
      raise FtpCredentialsFormatError()

  credential_file_content = ''
  ftp_credentials = {}
  try:
    credential_file_content = aws_utils.get_s3_file_content(ftp_credentials_bucket_name, credential_file_name)
    credetial_lines = [line.split(':', 1) for line in credential_file_content.splitlines()]
    ftp_credentials = dict([(key.lower().strip(), value.strip("'").strip()) for (key, value) in credetial_lines])
    if show_output:
      print(f'Got the ftp credentials successfully from bucket "{ftp_credentials_bucket_name}" and file "{credential_file_name}":', ftp_credentials)
  except:
    if show_output:
      print('The ftp credentials file content is invalid to parse! Check below:')
      print(credential_file_content)
    if exit_on_failed:
      sys.exit(0)
    else:
      raise FtpCredentialsFormatError()

  ftp_utils = SftpUtils(ftp_credentials['address'], ftp_credentials['username'], ftp_credentials['password'], port=sftp_port)
  return ftp_credentials, ftp_utils

# get pgp key from bucket
def get_pgp_key_to_decrypt(show_output=True, exit_on_failed=True):
  aws_utils = get_aws_utils()
  downloaded_file_name = ''

  try:
    downloaded_file_name = aws_utils.download_s3_file(pgp_s3_private_key_bucket, pgp_s3_private_key_file_path)
    if show_output:
      print(f'Got the decryption pgp key file from bucket "{pgp_s3_private_key_bucket}" successfully:', pgp_s3_private_key_file_path)
  except:
    if show_output:
      print(f'Could not download file "{pgp_s3_private_key_file_path}" from bucket "{pgp_s3_private_key_bucket}".')
    if exit_on_failed:
      sys.exit(0)

  return downloaded_file_name