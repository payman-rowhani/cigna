import os
import sys
sys.path.insert(0, '.')
sys.path.insert(1, '..')

from common.data.csv_utils import CsvUtils
from common.data.mysql_utils import MySqlUtils

# mysql db credentials
db_endpoint = 'ec2-35-170-191-97.compute-1.amazonaws.com'
db_username = 'admin'
db_password = 'admin'
db_name = 'initial'

# required import info
excel_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_excel_import_sample_data.xlsx')
db_table_name = 'person'

def run_insertion():
  csv_data_dict_list = CsvUtils(excel_file, is_excel=True).get_data_dict_list()
  db_loader = MySqlUtils(db_endpoint, db_username, db_password, db_name)
  invalid_field_names, num_of_rows_inserted = db_loader.insert_data_dict_list_to_db(db_table_name, csv_data_dict_list)

  if invalid_field_names:
    print('Invalid field names in csv/excel file:', ', '.join(invalid_field_names))
  print(f'{num_of_rows_inserted} records inserted successfully!')

if __name__ == '__main__':
  run_insertion()
