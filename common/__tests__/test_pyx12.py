
import pyx12
import pyx12.x12context
import pyx12.params
import pyx12.segment

input_file_name = 'sample_edi_834_input/XO16000__xo10001i.57484.20180710.edi'
input_file_name = 'sample_edi_834_input/sample.edi'

param = pyx12.params.params()
errh = pyx12.error_handler.errh_null()
with open(input_file_name, 'rt') as fd_in:
  src = pyx12.x12context.X12ContextReader(param, errh, fd_in)
  for datatree in src.iter_segments('1000A'): #HierarchicalLoop
    if datatree.id == '1000A':
      loop_1000A = datatree
      sponsor = loop_1000A.get_value('N102')
      print(sponsor)