import sys
sys.path.insert(0, '.')
sys.path.insert(1, '..')

from common.data.mysql_utils import MySqlUtils
from common.build.data_field import DataField
from common.build.csv_generator import CsvGenerator

# mysql db credentials
db_endpoint = 'ec2-35-170-191-97.compute-1.amazonaws.com'
db_username = 'admin'
db_password = 'admin'
db_name = 'initial'

# aws credentials
aws_region_name = 'us-east-1'
aws_access_key_id = 'AKIAICBIO225NQJACS6Q'
aws_secret_access_key = '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'
aws_s3_bucket_name = 'artmansoft-bucket'

# ftp server credentials
ftp_endpoint = 'ftp.drivehq.com'
ftp_username = 'paymanrowhani'
ftp_password = '7261416Artman'

# get data from mysql db
def __get_db_data():
  db_loader = MySqlUtils(db_endpoint, db_username, db_password, db_name)
  return db_loader.fetch_all_from_table('person')

# build converted data
def __build_data(db_dict_list):
  data_fields = [
    DataField('id'),
    DataField('name', 'Full Name', lambda name: name.upper()),
    DataField('age')
  ]
  return CsvGenerator(db_dict_list, data_fields)

# send converted data to file, s3 bucket and ftp server
def __send_converted_data(output_generator):
  file_name = 'result.csv'
  output_generator.write_output_to_file(file_name, write_header=True)
  output_generator.write_output_to_bucket(file_name, aws_s3_bucket_name, aws_region_name, aws_access_key_id, aws_secret_access_key, write_header=True)
  output_generator.write_output_to_ftp(file_name, ftp_endpoint, ftp_username, ftp_password, write_header=True)

def run_conversion():
  db_dict_list = __get_db_data()
  output_generator = __build_data(db_dict_list)
  __send_converted_data(output_generator)

if __name__ == '__main__':
  run_conversion()