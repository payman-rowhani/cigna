import os
import tempfile
import csv
import itertools
from datetime import datetime

class CsvUtils:
  """
    This class contains utilities to load csv/excel data
  """

  def __init__(self, file_path, is_excel=False):
    """
      Args:
        file_path (str): The path to read/write csv/excel file.
        is_excel? (bool): Whether the file is/should be in excel format (not csv).

      Returns:
        None
    """

    self.file_path = file_path
    self.is_excel = is_excel

  def get_data_dict_list(self, field_names=None, delimiter=',', date_fields_format='%Y%m%d', max_rows_count=0, row_offset=0, field_names_map=None):
    """
      Args:
        field_names? (list): List of field names to be used as dictionary keys (if null, the first row is used)
        delimiter? (str): Delimeter used for seperating columns
        date_fields_format? (str): Format of date fields
        max_rows_count? (int): Maximum number of rows to return (defaults to all rows)
        row_offset? (int): Number of rows to skip (defaults to 0)
        field_names_map? (dict): Map to replace field names
      Returns:
        Data dict list
    """

    csv_file_path = self.convert_excel_to_csv() if self.is_excel else self.file_path

    if field_names_map:
      field_names_map = dict((fieldname.lower(), value.strip()) for (fieldname, value) in field_names_map.items())

    data_dict_list = None
    with open(csv_file_path) as csv_file:
      reader = csv.DictReader(csv_file, fieldnames=field_names, delimiter=delimiter)
      data_dict_list = []
      for row in itertools.islice(reader, row_offset, (row_offset + max_rows_count) if max_rows_count else None):
        row = dict((fieldname.lower(), value.strip()) for (fieldname, value) in row.items())

        if field_names_map:
          for fieldname in field_names_map:
            fieldname = fieldname.lower()
            if fieldname in row:
              new_fieldname = field_names_map[fieldname]
              if fieldname != new_fieldname:
                row[new_fieldname] = row[fieldname]
                del row[fieldname]

        for fieldname in row:
          if fieldname.endswith('date'):
            if row[fieldname]:
              try:
                row[fieldname] = datetime.strptime(row[fieldname], date_fields_format)
              except:
                pass
            else:
              row[fieldname] = None

        data_dict_list.append(row)

    if self.is_excel and self.file_path != csv_file_path:
      os.remove(csv_file_path)

    return data_dict_list

  def write_data_dict_list(self, data_dict_list, write_header=False):
    """
      Args:
        data_dict_list (list): The list of dictionaries to be written to file.
        write_header? (bool): Whether header row should be written to csv.

      Returns:
        None
    """

    if self.is_excel:
      file_stream = tempfile.NamedTemporaryFile(mode='w', newline='', encoding='utf-8', suffix='.csv', delete=False)
      csv_file_path = file_stream.name
    else:
      file_stream = open(self.file_path, 'w', newline='', encoding='utf-8')
      csv_file_path = self.file_path

    field_names = data_dict_list[0].keys() if data_dict_list else []
    csv_writer = csv.DictWriter(file_stream, fieldnames=field_names)
    if write_header:
      csv_writer.writeheader()
    for row in data_dict_list:
      csv_writer.writerow(row)
    file_stream.close()

    if self.is_excel:
      self.convert_csv_to_excel(csv_file_path, field_names)
      os.remove(csv_file_path)

  def convert_excel_to_csv(self):
    """
      Returns:
        The path to csv file
    """

    import pandas

    csv_file_stream = tempfile.NamedTemporaryFile(mode='w', newline='', encoding='utf-8', suffix='.csv', delete=False)
    csv_file_path = csv_file_stream.name
    excel_file = pandas.read_excel(self.file_path, index_col=None, dtype=str).replace('nan', '')
    excel_file.to_csv(csv_file_path, encoding='utf-8', index=False)

    return csv_file_path

  def convert_csv_to_excel(self, csv_file_path, field_names=None):
    """
      Args:
        csv_file_path: The file path to the csv file.
        field_names?: The field names to be used as header.

      Returns:
        The path to excel file
    """

    import pandas
    import pandas.io.formats.excel
    from pandas.io.excel import ExcelWriter
    pandas.io.formats.excel.header_style = None

    with ExcelWriter(self.file_path, engine='xlsxwriter') as ew:
      df = pandas.read_csv(csv_file_path, dtype=str).replace('nan', '')
      df.to_excel(ew, index=False, columns=field_names, sheet_name='Worksheet')

      worksheet = ew.sheets['Worksheet']
      for i, col in enumerate(df.columns):
        column_len = df[col].astype(str).str.len().max()
        column_len = max(column_len, len(col)) + 3
        worksheet.set_column(i, i, column_len)
      ew.save()

    return self.file_path
