import pymysql
import warnings

class MySqlUtils:
  """
    This class contains utilities to load mysql data
  """

  def __init__(self, host_or_ip, username, password, schema_name, connect_timeout=30, use_single_connection=False):
    """
      Args:
        host_or_ip (str): The database endpoint address.
        username (str): The databse username.
        password (str): The databse password.
        schema_name (str): The databse schema to load.
        connect_timeout? (int): The optional database connection timeout in seconds (defaults to 30s).
        use_single_connection? (bool): If provided, a single connection is used for all queries.

      Returns:
        None
    """

    self.host_or_ip = host_or_ip
    self.username = username
    self.password = password
    self.schema_name = schema_name
    self.connect_timeout = connect_timeout
    self.use_single_connection = use_single_connection
    self.db_connection = None

  def get_db_connection(self):
    """
      Returns:
        MySQl db connection
    """

    if not self.db_connection or not self.use_single_connection:
      self.db_connection = pymysql.connect(self.host_or_ip,
                                          user=self.username,
                                          passwd=self.password,
                                          db=self.schema_name,
                                          connect_timeout=self.connect_timeout,
                                          cursorclass=pymysql.cursors.DictCursor)
    return self.db_connection

  def close_db_connection(self):
    """
      Returns:
        None
    """
    if self.db_connection:
      try:
        self.db_connection.close()
      except:
        pass

  def execute_query(self, query, args=None, execute_many=False, keep_cursor_open=False):
    """
      Args:
        query (str): The query to run.
        args? (any): Parameter used with query.
        execute_many? (bool): Whether multiple statements need to execute in query.
        keep_cursor_open? (bool): Whether the cursor should be kept open or not.

      Returns:
        The execution result and db cursor
    """

    conn = self.get_db_connection()
    cursor = conn.cursor()
    result = cursor.executemany(query, args=args) if execute_many else cursor.execute(query, args=args)
    conn.commit()
    if not keep_cursor_open:
      cursor.close()
      if not self.use_single_connection:
        conn.close()
    return (result, cursor)

  def fetch_all_from_query(self, query):
    """
      Args:
        query (str): The query to run.

      Returns:
        The fetched data
    """

    result, cursor = self.execute_query(query, keep_cursor_open=True)
    result = cursor.fetchall()
    cursor.close()
    return result

  def fetch_all_from_table(self, table_name):
    """
      Args:
        table_name (str): The db table name to fetch data from.

      Returns:
          The fetched data
    """

    query = f'SELECT * FROM {table_name}'
    result = self.fetch_all_from_query(query)
    return result

  def _get_valid_db_field_names(self, table_name, field_names):
    """
      Args:
        table_name (str): The db table name to check field names on.
        field_names (list): List of field names (db columns) to check if they exist in table

      Returns:
          Valid field names
    """

    query = f'SHOW COLUMNS FROM `{table_name}`'
    table_field_names = [col['Field'].lower() for col in self.fetch_all_from_query(query)]

    valid_field_names = []
    for field_name in field_names:
      if field_name.lower() in table_field_names:
        valid_field_names.append(field_name.lower())
    return valid_field_names

  def insert_data_dict_list_to_db(self, table_name, data_dict_list, ignored_fields=None, nullify_empty_values=True, ignore_warnings=True):
    """
      Args:
        table_name (str): The db table name to insert data into.
        data_dict_list (list): List of dictionaries (records) containing data to insert for each field (column).
        ignored_fields? (list): Optional list of colums to ignore while inserting
        nullify_empty_values? (bool): Optional flag to nullify empty values
        ignore_warnings? (bool): Optional flag to ignore sql warnings

      Returns:
          List of invalid field names
    """

    if not len(data_dict_list):
      return

    ignored_fields = set([f.lower() for f in (ignored_fields or [])])
    field_names = [f.lower() for f in data_dict_list[0].keys()]
    valid_field_names = list(set(self._get_valid_db_field_names(table_name, field_names)) - ignored_fields)
    invalid_field_names = list(set(field_names) - set(valid_field_names) - ignored_fields)

    keys_statement = f"({','.join([f'`{field}`' for field in valid_field_names])})"
    values_statement = f"({','.join(['%s' for field in valid_field_names])})"
    insert_statement = f"INSERT INTO `{table_name}` {keys_statement} VALUES {values_statement}"
    insert_args = []
    for record in data_dict_list:
      arg = [record[field] if not nullify_empty_values or record[field] != '' else None for field in valid_field_names]
      insert_args.append(arg)

    with warnings.catch_warnings():
      ignore_warnings and warnings.simplefilter('ignore')
      num_of_rows_affected = self.execute_query(insert_statement, insert_args, execute_many=True)[0]
      return invalid_field_names, num_of_rows_affected