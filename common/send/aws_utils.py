import os
import tempfile
import json
import requests
import boto3
from botocore.errorfactory import ClientError
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth

class DuplicateS3Key(Exception):
  pass

class AwsUtils:
  """
    This class contains utilities for aws cli resources
  """

  def __init__(self, region_name=None, aws_access_key_id=None, aws_secret_access_key=None):
    """
      Args:
        region_name? (str): AWS region name.
        aws_access_key_id? (str): AWS access key id.
        aws_secret_access_key? (str): AWS secret access key.

      Returns:
        None
    """

    self.region_name = region_name
    self.aws_access_key_id = aws_access_key_id
    self.aws_secret_access_key = aws_secret_access_key

  def __get_aws_client(self, resource_name):
    return boto3.client(resource_name,
                        region_name=self.region_name,
                        aws_access_key_id=self.aws_access_key_id,
                        aws_secret_access_key=self.aws_secret_access_key)

  def __get_aws_resource(self, resource_name):
    return boto3.resource(resource_name,
                        region_name=self.region_name,
                        aws_access_key_id=self.aws_access_key_id,
                        aws_secret_access_key=self.aws_secret_access_key)

  def get_s3_bucket_client(self):
    """
      Returns:
        AWS S3 Bucket client
    """

    return self.__get_aws_client('s3')

  def get_ec2_bucket_client(self):
    """
      Returns:
        AWS EC2 client
    """

    return self.__get_aws_client('ec2')

  def get_lambda_bucket_client(self):
    """
      Returns:
        AWS Lambda client
    """

    return self.__get_aws_client('lambda')

  def get_s3_bucket_resource(self):
    """
      Returns:
        AWS S3 Bucket resource
    """

    return self.__get_aws_resource('s3')

  def get_ec2_bucket_resource(self):
    """
      Returns:
        AWS EC2 resource
    """

    return self.__get_aws_resource('ec2')

  def get_lambda_bucket_resource(self):
    """
      Returns:
        AWS Lambda resource
    """

    return self.__get_aws_resource('lambda')

  def get_es_instance(self, host, port=443, timeout=60):
    """
      Args:
        host (str): ES host.
        port? (int): ES port.
        timeout? (int): Connection timeout in seconds.

      Returns:
        AWS Elasticsearch instance
    """

    awsauth = AWS4Auth(self.aws_access_key_id, self.aws_secret_access_key, self.region_name, 'es')
    return Elasticsearch(
      hosts=[f'{host}:{port}'],
      http_auth=awsauth,
      use_ssl=False,
      verify_certs=True,
      connection_class=RequestsHttpConnection,
      timeout=timeout)

  def upload_file_to_s3(self, bucket_name, file_path_to_upload, file_key_on_bucket=None, error_if_duplicate_key=True):
    """
      Args:
        bucket_name: The bucket name to upload to.
        file_path_to_upload (str): The path to the file to upload.
        file_key_on_bucket? (str): The name of the key to upload to on bucket.
        error_if_duplicate_key? (bool): Whether should raise exception of the key is already existing

      Returns:
        None
    """

    if not file_key_on_bucket:
      file_key_on_bucket = os.path.basename(file_path_to_upload)

    s3_client = self.get_s3_bucket_client()
    if error_if_duplicate_key:
      try:
        s3_client.head_object(Bucket=bucket_name, Key=file_key_on_bucket)
        raise DuplicateS3Key
      except ClientError:
        # Not found
        pass
    s3_client.upload_file(file_path_to_upload, bucket_name, file_key_on_bucket)

  def s3_file_exists(self, bucket_name, file_name):
    """
    Args:
      bucket_name: Name of the S3 bucket.
      file_name: The file name (key) to check its existence.

    Returns:
      The boolean indicating whether the file exists
    """

    s3_client = self.get_s3_bucket_client()
    try:
      s3_client.head_object(Bucket=bucket_name, Key=file_name)
      return True
    except:
      return False

  def get_s3_file_list(self, bucket_name, prefix='', suffix='', contains=''):
    """
    Args:
      bucket_name: Name of the S3 bucket.
      prefix?: Only fetch files (keys) that start with this prefix (optional).
      suffix?: Only fetch files (keys) that end with this suffix (optional).
      contains?: Only fetch files (keys) that contains this text (optional).

    Returns:
      The list of matching files (keys) on the bucket sorted by last modified descending
    """

    s3_client = self.get_s3_bucket_client()

    kwargs = {'Bucket': bucket_name}
    if prefix:
      kwargs['Prefix'] = prefix

    objects = []
    while True:
      resp = s3_client.list_objects_v2(**kwargs)
      objects.extend(resp['Contents'])

      try:
        kwargs['ContinuationToken'] = resp['NextContinuationToken']
      except KeyError:
        break

    file_list = []
    for obj in sorted(objects, key=lambda obj: obj['LastModified'].strftime('%Y-%m-%d %H:%M:%S+%H:%M'), reverse=True):
      key = obj['Key']
      if key.startswith(prefix) and key.endswith(suffix) and contains in key and not key.endswith('/'):
        file_list.append(key)
    return file_list

  def get_s3_file_content(self, bucket_name, file_name):
    """
    Args:
      bucket_name: Name of the S3 bucket.
      file_name: The file name (key) to read content from.

    Returns:
      The content of s3 file
    """

    s3_client = self.get_s3_bucket_client()
    s3_file = s3_client.get_object(Bucket=bucket_name, Key=file_name)
    return s3_file["Body"].read().decode('utf8')

  def download_s3_file(self, bucket_name, file_name, download_as=None):
    """
    Args:
      bucket_name: Name of the S3 bucket.
      file_name: The file name (key) to download.
      donwload_as?: The file path to save the downloaded file (optional)

    Returns:
      The path to the downloaded file
    """

    if not download_as:
      file_stream = tempfile.NamedTemporaryFile(mode='wt', newline='', encoding='utf-8', delete=False)
      download_as = file_stream.name
      file_stream.close()

    s3_client = self.get_s3_bucket_client()
    s3_client.download_file(Bucket=bucket_name, Key=file_name, Filename=download_as)
    return download_as

  def __download_s3_dir_helper(self, s3_client, s3_bucket_name, s3_top_level_source_folder, dest_dir, recursive=True, show_output=True, s3_current_source_folder=None):
    s3_current_source_folder = s3_top_level_source_folder if s3_current_source_folder is None else s3_current_source_folder
    paginator = s3_client.get_paginator('list_objects')
    for s3_objects_list in paginator.paginate(Bucket=s3_bucket_name, Delimiter='/', Prefix=s3_current_source_folder):
      if recursive:
        s3_folders = s3_objects_list.get('CommonPrefixes')
        if s3_folders is not None:
          for subfolder in s3_folders:
            for downloaded_file_path in self.__download_s3_dir_helper(s3_client, s3_bucket_name, s3_top_level_source_folder, dest_dir, recursive=True, show_output=show_output, s3_current_source_folder=subfolder.get('Prefix')):
              yield downloaded_file_path

      s3_file_objects = [obj for obj in s3_objects_list.get('Contents') or [] if not obj['Key'].endswith('/')]
      for s3_file_object in s3_file_objects:
        s3_object_key = s3_file_object['Key']
        dest_path_of_file = os.path.realpath(os.path.join(dest_dir, s3_object_key[len(s3_top_level_source_folder):]))
        dest_folder_of_file = os.path.dirname(dest_path_of_file)
        if not os.path.exists(dest_folder_of_file):
          if show_output:
            print('Creating local directory "%s"' % (dest_folder_of_file,))
          os.makedirs(dest_folder_of_file)
        with open(dest_path_of_file, 'wb') as local_file_handle:
          s3_client.download_fileobj(s3_bucket_name, s3_object_key, local_file_handle)
          if show_output:
            print('Finished download of file "%s" from bucket "%s" to local file "%s"' % (s3_object_key, s3_bucket_name, dest_path_of_file))

        yield dest_path_of_file

  def download_s3_dir(self, bucket_name, bucket_top_level_source_folder, local_dest_dir, recursive=True, show_output=True):
    """
    Args:
      bucket_name: Name of the S3 bucket.
      bucket_top_level_source_folder: The top level directory on s3 bucket to download (use empty string to copy the whole bucket).
      local_dest_dir: The local destination folder (all files will be overwritten).
      recursive?: If set, the folder structure of bucket_top_level_source_folder will be recreated inside local_dest_dir with copying performed recursively (defaults to True).
      show_output? : If set, verbose statements will be printed

    Returns:
      The path to the downloaded file
    """

    """Downloads all objects in the given S3 bucket and source folder into the given local destination folder.
       If recursive=True, the folder structure of s3_top_level_source_folder will be recreated inside local_dest_folder with copying performed recursively."""

    s3_client = self.get_s3_bucket_client()
    return list(self.__download_s3_dir_helper(s3_client, bucket_name, bucket_top_level_source_folder, local_dest_dir, recursive, show_output))

  def create_es_index(self, host, index, doc_type, id, body, port=443, timeout=60):
    """
      Args:
        host (str): ES host.
        index (str): The ES index.
        doc_type (str): The ES document type.
        id (str): The entry id.
        body (obj): The entry body.
        port?: The ES port.
        timeout?: Connection timeout in seconds.

      Returns:
        None
    """

    es = self.get_es_instance(host, port, timeout)
    es.index(index=index, doc_type=doc_type, id=id, body=body)
    #requests.put(f'{es_host}/{index}/{doc_type}/{id}', auth=awsauth, json=body)

  def create_bulk_es_index(self, host, index, doc_type, data_list, bulk_size=100, port=443, timeout=60, show_output=True):
    """
      Args:
        host (str): ES host.
        index (str): The ES index.
        doc_type (str): The ES document type.
        data_list (list): The entries to insert.
        bulk_size? (int): Number of entries to create index for in each run (0 means all at once).
        port?: The ES port.
        timeout?: Connection timeout in seconds.
        show_output? : If set, verbose statements will be printed.

      Returns:
        None
    """

    es = self.get_es_instance(host, port, timeout)
    bulk_strings = []
    id = 1
    bulk = 1

    def print_bulk_output():
      if show_output:
        if not bulk_size:
          print('Creating ES indices as a single bulk...')
        else:
          print(f'Creating ES indices for bulk {bulk} (size: {bulk_size})...')

    def insert_bulk():
      es.bulk('\n'.join(bulk_strings))

    print_bulk_output()
    for item in data_list:
      bulk_strings.append(json.dumps({"index": {"_index": index, "_type": doc_type, "_id": str(id)}}))
      bulk_strings.append(json.dumps(item))
      if bulk_size and (id % bulk_size == 0 or id == len(data_list)):
        insert_bulk()
        bulk_strings = []
        bulk += 1
        if id != len(data_list):
          print_bulk_output()
      id += 1
    if not bulk_size:
      insert_bulk()
    if show_output:
      print(f'Created ES index for {id-1} items successfully.')