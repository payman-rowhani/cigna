import pysftp
import os

class SftpUtils:
  """
    This class contains utilities for sftp
  """

  def __init__(self, host_or_ip, username=None, password=None, private_key=None, private_key_pass=None, port=22):
    """
      Args:
        host_or_ip (str): SFTP server endpoint.
        username? (str): SFTP server username.
        password? (str): SFTP server password.
        private_key? (str): Path to the private key used for SFTP server.
        private_key_pass? (str): Password of encrypted private key used for SFTP server.
        port? (int): SFTP server port.

      Returns:
        None
    """

    self.host_or_ip = host_or_ip
    self.port = port
    self.username = username
    self.password = password
    self.private_key = private_key
    self.private_key_pass = private_key_pass

  def upload_file(self, file_path_to_upload, file_path_on_server=None, create_directory_if_needed=False):
    """
      Args:
        file_path_to_upload (str): The path to the file to upload.
        file_path_on_server? (str): The file path to save on sftp server.
        create_directory_if_needed? (bool): If set, it will create the directories if not existing

      Returns:
        None
    """

    if not file_path_on_server:
      file_path_on_server = os.path.join('.', os.path.basename(file_path_to_upload))
    server_directory = os.path.dirname(file_path_on_server)

    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    with pysftp.Connection(self.host_or_ip, username=self.username, password=self.password, private_key=self.private_key, private_key_pass=self.private_key_pass, port=self.port, cnopts=cnopts) as sftp:
      if create_directory_if_needed and server_directory != '.' and not sftp.exists(server_directory):
        sftp.makedirs(server_directory)
      sftp.put(file_path_to_upload, file_path_on_server)

  def download_all_files(self, ftp_directory, download_directory, suffix='', skip_file_function=None, show_output=False):
    """
      Args:
        ftp_directory (str): The path to the directory to get files from.
        download_directory (str): The path to save the files to.
        suffix?: Only fetch files that end with this suffix (optional).
        skip_file_function?: A function to skip files if not needed (optional).
        show_output? (bool): If set, verbose data is shown (optional).

      Returns:
        List of downloaded files
    """

    if not os.path.exists(download_directory):
      os.makedirs(download_directory)

    downloaded_file_list = []

    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    with pysftp.Connection(self.host_or_ip, username=self.username, password=self.password, private_key=self.private_key, private_key_pass=self.private_key_pass, port=self.port, cnopts=cnopts) as sftp:
      ftp_directory = ftp_directory.rstrip('/') + '/'
      for filename in sftp.listdir(ftp_directory):
        filename_with_directory = ftp_directory + filename
        if sftp.isfile(filename_with_directory) and filename.lower().endswith(suffix.lower()) and (not skip_file_function or not skip_file_function(filename)):
          saved_file = os.path.join(download_directory, filename)
          downloaded_file_list.append(saved_file)
          sftp.get(filename_with_directory, saved_file)
          if show_output:
            print(f'Downloaded ftp file "{filename_with_directory}"')

    return downloaded_file_list
