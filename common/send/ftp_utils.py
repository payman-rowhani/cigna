import ftplib
import os

class FtpUtils:
  """
    This class contains utilities for ftp
  """

  def __init__(self, host_or_ip, username=None, password=None):
    """
      Args:
        host_or_ip (str): FTP server endpoint.
        username? (str): FTP server username.
        password? (str): FTP server password.

      Returns:
        None
    """

    self.host_or_ip = host_or_ip
    self.username = username
    self.password = password

  def upload_file(self, file_path_to_upload, file_path_on_server=None):
    """
      Args:
        file_path_to_upload (str): The path to the file to upload.
        file_path_on_server? (str): The file path to save on ftp server.

      Returns:
        None
    """

    if not file_path_on_server:
      file_path_on_server = os.path.basename(file_path_to_upload)

    with ftplib.FTP(self.host_or_ip, self.username, self.password) as ftp_writer:
      with open(file_path_to_upload, 'rb') as ftp_file:
        ftp_writer.storbinary('STOR ' + file_path_on_server, ftp_file)
