class DataField:
  """
    This class contains data for a data field
  """

  def __init__(self, field_name, field_title=None, val_formatter=None, val_len=None, default_val=None, no_uppercase_val=False):
    """
      Args:
        field_name (str): The data field name (can be null if constant is sent in default_val).
        field_title? (str): The optional column field_title (an alternative human-readable name mostly for demonstration).
        val_formatter? (function/dict): The optional value formatter function/dict to format/map field value.
        val_len? (int): The optional value length
        default_val? (str): The optional default value for this field to use when no mapping is found
        no_uppercase_val? (bool): The optional flag to indicate that the value should not be converted to uppercase string

      Returns:
        None
    """

    self.field_name = field_name
    self.field_title = field_title or self.field_name
    self.val_formatter = val_formatter
    self.val_len = val_len
    self.default_val = default_val
    self.no_uppercase_val = no_uppercase_val

  def get_formatted_value(self, field_value, no_uppercase_val_override=False):
    """
      Args:
        field_value (str): The field value for the matched field name.
        no_uppercase_val_override? (bool): If set, value is not uppercased (defaults to False)

      Returns:
        The formatted value (str)
    """

    default_value = field_value if field_value is not None and field_value != '' else self.default_val

    if self.val_formatter is not None:
      if isinstance(self.val_formatter, dict):
        self.val_formatter = {k.lower() if isinstance(k, str) else k: v for k, v in self.val_formatter.items()}
        field_value_as_key = default_value
        if field_value_as_key is not None and isinstance(field_value_as_key, str):
          field_value_as_key = field_value_as_key.lower()
        formatted_value = self.val_formatter.get(field_value_as_key, default_value)
      else:
        formatted_value = self.val_formatter(default_value)
        if formatted_value is None:
          formatted_value = default_value
    else:
      formatted_value = default_value

    formatted_value = '' if formatted_value is None else str(formatted_value)
    if self.val_len is not None:
      if len(formatted_value) < self.val_len:
        formatted_value += ' ' * (self.val_len - len(formatted_value))
      elif len(formatted_value) > self.val_len:
        formatted_value = formatted_value[:self.val_len]

    return formatted_value if self.no_uppercase_val or no_uppercase_val_override else formatted_value.upper()
