import os
import tempfile
import csv
from abc import ABC, abstractmethod

class GeneratorBase(ABC):
  """
    This class is the base class for all generator classes and provide common functionalities
  """

  def __init__(self, data_dict_list, data_fields, file_suffix, file_mode):
    """
      Args:
        data_dict_list (list): The list of dictionaries containing field names and values.
        data_fields (list): List of DataField elements to be generated.
        file_suffix (str): Temporary file suffix (e.g. txt, csv)
        file_mode: File mode to write (w, wt, wb)

      Returns:
        None
    """

    self.data_dict_list = data_dict_list
    self.data_fields = data_fields
    self.file_suffix = file_suffix
    self.file_mode = file_mode

  def generate_data_definiton_table(self, file_path):
    """
      Args:
        file_path (str): CSV file path for the definition table generated from list of data fields (field name, description, length, default value).

      Returns:
        None
    """

    with open(file_path, 'w', newline='', encoding='utf-8') as file_stream:
      headers = ['Name', 'Description', 'Start', 'Length', 'End', 'Default']
      csv_writer = csv.DictWriter(file_stream, fieldnames=headers)
      csv_writer.writeheader()
      start_pos = 1
      end_pos = 0
      for data_field in self.data_fields:
        if end_pos != '?' and data_field.val_len is not None:
          end_pos += data_field.val_len
        else:
          end_pos = '?'
        csv_writer.writerow({
          'Name': data_field.field_name or '?',
          'Description': data_field.field_title,
          'Start': start_pos,
          'Length': data_field.val_len if data_field.val_len is not None else '?',
          'End': end_pos,
          'Default': repr(data_field.default_val) if data_field.default_val else (not data_field.field_name and 'NULL') or ''
        })
        if start_pos != '?' and data_field.val_len is not None:
          start_pos += data_field.val_len
        else:
          start_pos = '?'

  def _get_field_value(self, data_row, data_field, no_uppercase_val=False):
    """
      Args:
        data_row (dict): a row of data in db (record).
        data_field (DataField): a data field represanting one db column metadata.
        no_uppercase_val? (bool): If set, value is not uppercased (defaults to False)

      Returns:
        None
    """

    field_value = data_row.get(data_field.field_name)
    return str(data_field.get_formatted_value(field_value, no_uppercase_val))

  @abstractmethod
  def write_output_to_stream(self, file_stream, **kwargs):
    """
      Args:
        file_stream (file-like stream): The file stream to save the generated file.

      Returns:
        None
    """

    raise NotImplementedError('write_output_to_stream not implemented!')

  def write_output_to_file(self, file_path=None, **kwargs):
    """
      Args:
        file_path? (str): The file path to save the generated file. If not set, a temp file is generated.

      Returns:
        The generated file path
    """

    if file_path:
      file_stream = open(file_path, self.file_mode, newline='', encoding='utf-8')
    else:
      file_stream = tempfile.NamedTemporaryFile(mode=self.file_mode, newline='', encoding='utf-8', suffix=self.file_suffix, delete=False)
      file_path = file_stream.name

    self.write_output_to_stream(file_stream, **kwargs)

    return file_path

  def write_output_to_bucket(self, bucket_file_path, bucket_name, aws_region_name=None, aws_access_key_id=None, aws_secret_access_key=None, **kwargs):
    """
      Args:
        bucket_name (str): The s3 bucket name.
        bucket_file_path (str): The file path to save on s3 bucket.
        aws_region_name? (str): AWS region name.
        aws_access_key_id? (str): AWS access key id.
        aws_secret_access_key? (str): AWS secret access key.

      Returns:
        None
    """

    from common.send.aws_utils import AwsUtils

    aws_utils = AwsUtils(aws_region_name, aws_access_key_id, aws_secret_access_key)
    output_file_path = self.write_output_to_file(**kwargs)
    aws_utils.upload_file_to_s3(bucket_name, output_file_path, bucket_file_path)
    os.remove(output_file_path)

  def write_output_to_ftp(self, ftp_file_path, ftp_host_or_ip, ftp_username=None, ftp_password=None, **kwargs):
    """
      Args:
        ftp_file_path (str): The file path to save on ftp server.
        ftp_host_or_ip (str): FTP server endpoint.
        ftp_username? (str): FTP server username.
        ftp_password? (str): FTP server password.

      Returns:
        None
    """

    from common.send.ftp_utils import FtpUtils

    ftp_utils = FtpUtils(ftp_host_or_ip, ftp_username, ftp_password)
    output_file_path = self.write_output_to_file(**kwargs)
    ftp_utils.upload_file(output_file_path, ftp_file_path)
    os.remove(output_file_path)

  def write_output_to_sftp(self, sftp_file_path, sftp_host_or_ip, sftp_username=None, sftp_password=None, sftp_private_key=None, sftp_private_key_pass=None, **kwargs):
    """
      Args:
        sftp_file_path (str): The file path to save on sftp server.
        sftp_host_or_ip (str): SFTP server endpoint.
        sftp_username? (str): SFTP server username.
        sftp_password? (str): SFTP server password.
        sftp_private_key? (str): Path to the private key used for SFTP server.
        sftp_private_key_pass? (str): Password of encrypted private key used for SFTP server.

      Returns:
        None
    """

    from common.send.sftp_utils import SftpUtils

    sftp_utils = SftpUtils(sftp_host_or_ip, sftp_username, sftp_password, sftp_private_key, sftp_private_key_pass)
    output_file_path = self.write_output_to_file(**kwargs)
    sftp_utils.upload_file(output_file_path, sftp_file_path)
    os.remove(output_file_path)