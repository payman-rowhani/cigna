import os
import csv
import tempfile

from common.build.generator_base import GeneratorBase

class CsvGenerator(GeneratorBase):
  """
    This class contains utilities to generate CSV file from a dictionary
  """

  def __init__(self, data_dict_list, data_fields, delimiter=',', save_as_excel=False, no_uppercase_vals=True):
    """
      Args:
        data_dict_list (list): The list of dictionaries containing field names and values.
        data_fields (list): List of DataField elements to be generated.
        delimiter? (str): Delimiter (defaults to comma).
        save_as_excel? (bool): If set, excel file is generated (defaults to False).
        no_uppercase_vals? (bool): If set, values are not uppercased (defaults to True)

      Returns:
        None
    """

    super().__init__(data_dict_list, data_fields, '.csv', 'w')
    self.delimiter = delimiter
    self.save_as_excel = save_as_excel
    self.no_uppercase_vals = no_uppercase_vals

  def get_csv_header_field_names(self):
    """
      Returns:
        List of csv header fields
    """

    return [data_field.field_title for data_field in self.data_fields]

  def _get_rendered_data(self):
    """
      Returns:
        List of csv rows each containing a dictionary of all data field key/value values.
    """

    output_rows = []
    for data_row in self.data_dict_list:
      record = {}
      for data_field in self.data_fields:
        record[data_field.field_title] = (self._get_field_value(data_row, data_field, self.no_uppercase_vals) or '').strip()
      output_rows.append(record)
    return output_rows

  def write_output_to_stream(self, file_stream, write_header=False):
    """
      Args:
        file_stream (file-like stream): The file stream to save the generated file.
        write_header? (bool): Whether header row should be written to csv.

      Returns:
        None
    """

    field_names = self.get_csv_header_field_names()
    csv_writer = csv.DictWriter(file_stream, fieldnames=field_names, delimiter=self.delimiter)

    if write_header:
      csv_writer.writeheader()

    data_rows = self._get_rendered_data()
    for row in data_rows:
      csv_writer.writerow(row)
    file_stream.close()

  def write_output_to_file(self, file_path=None, write_header=False, **kwargs):
    """
      Args:
        file_path? (str): The file path to save the generated file. If not set, a temp file is generated.
        write_header? (bool): Whether header row should be written to csv.

      Returns:
        The generated file path
    """

    if self.save_as_excel:
      csv_file_path = super().write_output_to_file(None, write_header=write_header, **kwargs)
      from common.data.csv_utils import CsvUtils
      if not file_path:
        file_stream = tempfile.NamedTemporaryFile(mode='w', newline='', encoding='utf-8', suffix='.xlsx', delete=False)
        file_path = file_stream.name
        file_stream.close()
      elif not file_path.lower().endswith('.xlsx'):
        file_path += '.xlsx'
      CsvUtils(file_path, is_excel=True).convert_csv_to_excel(csv_file_path)
      os.remove(csv_file_path)
      return file_path
    else:
      return super().write_output_to_file(file_path, write_header=write_header, **kwargs)

  def write_output_to_bucket(self, bucket_file_path, bucket_name, aws_region_name=None, aws_access_key_id=None, aws_secret_access_key=None, write_header=False, **kwargs):
    """
      Args:
        bucket_name (str): The s3 bucket name.
        bucket_file_path (str): The file path to save on s3 bucket.
        aws_region_name? (str): AWS region name.
        aws_access_key_id? (str): AWS access key id.
        aws_secret_access_key? (str): AWS secret access key.
        write_header? (bool): Whether header row should be written to csv.

      Returns:
        None
    """

    return super().write_output_to_bucket(bucket_file_path, bucket_name, aws_region_name, aws_access_key_id, aws_secret_access_key, write_header=write_header, **kwargs)

  def write_output_to_ftp(self, ftp_file_path, ftp_host_or_ip, ftp_username=None, ftp_password=None, write_header=False, **kwargs):
    """
      Args:
        ftp_file_path (str): The file path to save on ftp server.
        ftp_host_or_ip (str): FTP server endpoint.
        ftp_username? (str): FTP server username.
        ftp_password? (str): FTP server password.
        write_header? (bool): Whether header row should be written to csv.

      Returns:
        None
    """

    return super().write_output_to_ftp(ftp_file_path, ftp_host_or_ip, ftp_username, ftp_password, write_header=write_header, **kwargs)

  def write_output_to_sftp(self, sftp_file_path, sftp_host_or_ip, sftp_username=None, sftp_password=None, sftp_private_key=None, sftp_private_key_pass=None, write_header=False, **kwargs):
    """
      Args:
        sftp_file_path (str): The file path to save on sftp server.
        sftp_host_or_ip (str): SFTP server endpoint.
        sftp_username? (str): SFTP server username.
        sftp_password? (str): SFTP server password.
        sftp_private_key? (str): Path to the private key used for SFTP server.
        sftp_private_key_pass? (str): Password of encrypted private key used for SFTP server.
        write_header? (bool): Whether header row should be written to csv.

      Returns:
        None
    """

    return super().write_output_to_sftp(sftp_file_path, sftp_host_or_ip, sftp_username, sftp_password, sftp_private_key, sftp_private_key_pass, write_header=write_header, **kwargs)