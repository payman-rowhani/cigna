import os
from common.build.generator_base import GeneratorBase

class FixedLengthGenerator(GeneratorBase):
  """
    This class contains utilities to generate fixed-length file from a dictionary
  """

  def __init__(self, data_dict_list, data_fields):
    """
      Args:
        data_dict_list (list): The list of dictionaries containing field names and values.
        data_fields (list): List of DataField elements to be generated.

      Returns:
        None
    """

    super().__init__(data_dict_list, data_fields, '.txt', 'wt')

  def _get_rendered_data(self):
    """
      Returns:
        List of concatenated strings of all field values.
    """

    output_rows = []
    for data_row in self.data_dict_list:
      record = ''
      for data_field in self.data_fields:
        record += self._get_field_value(data_row, data_field)
      output_rows.append(record)
    return output_rows

  def write_output_to_stream(self, file_stream):
    """
      Args:
        file_stream (file-like stream): The file stream to save the generated file.

      Returns:
        None
    """

    data_rows = self._get_rendered_data()
    lines = os.linesep.join(data_rows)
    file_stream.write(lines)
    file_stream.close()