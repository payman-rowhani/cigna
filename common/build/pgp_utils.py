# download ftp://ftp.gnupg.org/gcrypt/binary/gnupg-w32cli-1.4.9.exe and put "C:\Program Files (x86)\GNU\GnuPG"
import gnupg
import os
from pprint import pprint

class PgpUtils:
  """
    This class contains utilities for pgp decryption/encryption
  """

  def __init__(self, public_key_path=None, private_key_path=None, passphrase=None, encrypted_file_extension='.pgp'):
    """
      Args:
        public_key_path? (str): The path to public key file used for encryption.
        private_key_path? (str): The path to private key file used for deycription.
        passphrase? (str): The pass phrase to use.
        encrypted_file_extension? (str): The encrypted file extension.

      Returns:
        None
    """

    self.public_key_path = public_key_path
    self.private_key_path = private_key_path
    self.passphrase = passphrase
    self.encrypted_file_extension = encrypted_file_extension

  def decrypt_files(self, input_file_list, output_dir, show_output=True, show_pgp_output=False):
    """
      Args:
        input_file_list (list): List of encrypted file paths to decrypt.
        output_dir (str): The path to save the decrypted files.
        show_output? (bool): If set, verbose output is shown.
        show_pgp_output? (bool): If set, pgp outputs will be printed.

      Returns:
        None
    """

    gpg = gnupg.GPG()
    import_result = gpg.import_keys(open(self.private_key_path).read())
    if show_pgp_output:
      print ('import key result:')
      pprint(import_result.results)
      private_keys = gpg.list_keys(secret=True)
      print ('private keys:')
      pprint(private_keys)

    decrypted_file_list = []
    for file_path in input_file_list:
      main_file_path = file_path
      with open(file_path, 'rb') as encrypted_file:
        if file_path.lower().endswith(self.encrypted_file_extension):
          file_path = os.path.splitext(file_path)[0]
        decrypted_file = os.path.join(output_dir, os.path.basename(file_path))

        decryption_status = gpg.decrypt_file(encrypted_file, always_trust=True, passphrase=self.passphrase, output=decrypted_file)
        if show_pgp_output:
          print('ok: ', decryption_status.ok)
          print('status: ', decryption_status.status)

        decrypted_file_list.append(decrypted_file)
        if show_output:
          print(f'Decrypted {os.path.basename(main_file_path)} to {os.path.basename(decrypted_file)}')

    return decrypted_file_list