from common.build.edi_parser import X12_5010_X220A1 # add more if needed *******
from common.build.edi_parser.extras import standard_segment
from common.build.edi_parser.X12.parse import Loop
from common.build.edi_parser.X12.parse import Message
from common.build.edi_parser.X12.parse import ParseError
from common.build.edi_parser.X12.parse import Properties

# Transaction Set ID -> X12VersionTuple -> [(parser module, parser name)]
PARSER_MAP = {
    # add more if needed *******
    '834': {
        X12_5010_X220A1: [('M834_5010_X220_A1', 'parsed_834')],
    }
}

def get_parsers(transaction_set_id, version_tuple):
    """
    Generate the parsers to try for a given transaction_set and version.

    The parsers should be tried in the given order.
    """
    try:
        parsers = PARSER_MAP[transaction_set_id][version_tuple]
    except KeyError:
        raise ValueError("Unsupported transaction set and version.",
                         transaction_set_id, version_tuple)

    for index, (module_name, parser_name) in enumerate(parsers):
        module = __import__('common.build.edi_parser.parsers.' + module_name,
                            fromlist=[parser_name])
        yield getattr(module, parser_name)


class SimpleParser(object):
    """
    A parser for a particular transaction set and version.
    """
    def __init__(self, transaction_set_id, version_tuple):
        self.transaction_set_id = transaction_set_id
        self.version_tuple = version_tuple

    def unmarshall(self, x12_contents, **kwargs):
        for parser in get_parsers(self.transaction_set_id, self.version_tuple):
            try:
                return parser.unmarshall(x12_contents, **kwargs)
            except ParseError:
                continue
        else:
            # Let the last parser raise.
            return parser.unmarshall(x12_contents, **kwargs)


STLoop = Loop(
    u'ST_LOOP',
    Properties(
        position=u'0200',
        looptype=u'explicit',
        repeat=u'>1',
        req_sit=u'R',
        desc=u'Transaction Set Header',
    ),
    standard_segment.st,
)

GSLoop = Loop(
    u'GS_LOOP',
    Properties(
        position=u'0200',
        looptype=u'explicit',
        repeat=u'>1',
        req_sit=u'R',
        desc=u'Functional Group Header',
    ),
    standard_segment.gs,
    STLoop,
)

ISALoop = Loop(
    u'ISA_LOOP',
    Properties(
        position=u'0010',
        looptype=u'explicit',
        repeat=u'>1',
        req_sit=u'R',
        desc=u'Interchange Control Header',
    ),
    standard_segment.isa,
    GSLoop,
)

ControlParser = Message(
    u'ASC X12 Interchange Control Structure',
    Properties(
        desc=u'ASC X12 Control Structure',
    ),
    ISALoop,
)


def parse_control_headers(x12_contents):
    """Parse only the control headers of an X12 message.

    This parses the three outer control headers of the given X12 message:

    * Interchange Control (ISA)
    * Functional Group (GS)
    * Transaction Set (ST).

    These can be used to identify the X12 versions and transaction set types
    contained in the message.
    """
    return ControlParser.unmarshall(x12_contents, ignoreExtra=True)
