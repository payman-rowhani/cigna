from decimal import Decimal
import json

from common.build.edi_parser.facade import X12LoopBridge
from common.build.edi_parser.facade import X12SegmentBridge
from common.build.edi_parser.facade import ElementAccess
from common.build.edi_parser.facade import ElementSequenceAccess
from common.build.edi_parser.facade import CompositeAccess
from common.build.edi_parser.facade import SegmentSequenceAccess
from common.build.edi_parser.facade import SegmentConversion
from common.build.edi_parser.facade import D8
from common.build.edi_parser.facade import Money
from common.build.edi_parser.facade import Facade
from common.build.edi_parser.facade import _to_python_dict
from common.build.edi_parser.facade import enum
from common.build.edi_parser.facade.common import ClaimAdjustment
from common.build.edi_parser.facade.common import ContactDetails
from common.build.edi_parser.facade.common import NamedEntity
from common.build.edi_parser.facade.enums.remittance import remittance_advice_codes
from common.build.edi_parser.facade.utils import first


class BaseLoop(X12LoopBridge):
  def __init__(self, aLoop, *args, **kwargs):
    super(BaseLoop, self).__init__(aLoop, *args, **kwargs)

  def __str__(self):
    # return self.to_json(indent=2)
    fields = filter(lambda k: not k.startswith('_') and k != 'loopName', self.__class__.__dict__)
    data = dict([(k, str(getattr(self, k))) for k in fields])
    return json.dumps(data, indent=2)


class Sponsor(BaseLoop):
  """Sponsor (Client) information from the 1000A loop."""

  loopName = "1000A"

  name = ElementAccess("N1", 2)
  federal_taxpayer_id = ElementAccess("N1", 4)

  def __init__(self, aLoop, *args, **kwargs):
    super(Sponsor, self).__init__(aLoop, *args, **kwargs)


class Payer(BaseLoop):
  """Payer (Insurer) information from the 1000B loop."""

  loopName = "1000B"

  name = ElementAccess("N1", 2)
  federal_taxpayer_id = ElementAccess("N1", 4)

  def __init__(self, aLoop, *args, **kwargs):
    super(Payer, self).__init__(aLoop, *args, **kwargs)


class MemberLevel(BaseLoop):
  """Member Level Detail information from the 2000 loop."""

  loopName = "2000"

  response_code = ElementAccess("INS", 1, x12type=enum({
      "Y": "Subscriber (Employee)",
      "N": "Non-Subscriber (Dependent)"}))
  relationship_code = ElementAccess("INS", 2, x12type=enum({
      "01": "Spouse",
      "18": "Employee",
      "19": "Child"}))
  benefit_status_code = ElementAccess("INS", 5, x12type=enum({
      "A": 'Active',
      "S": "Surviving Insured",
      "C": "COBRA"}))
  medicare_plan_code = ElementAccess("INS", 6, x12type=enum({
      "A": 'Medicare Part A',
      "B": 'Part B',
      "C": 'Parts A&B',
      "D": 'Medicare',
      "E": "No Medicare"}))
  subscriber_ssn = ElementAccess("REF", 2)

  def __init__(self, aLoop, *args, **kwargs):
    super(MemberLevel, self).__init__(aLoop, *args, **kwargs)


class Member(BaseLoop):
  """Member (Insured or Subscriber) information from the 2100A loop."""

  loopName = "2100A"

  last_name = ElementAccess("NM1", 3)
  first_name = ElementAccess("NM1", 4)
  middle_name = ElementAccess("NM1", 5)
  member_ssn = ElementAccess("NM1", 9)
  address_line1 = ElementAccess("N3", 1)
  address_line2 = ElementAccess("N3", 2)
  city = ElementAccess("N4", 1)
  state = ElementAccess("N4", 2)
  zip_code = ElementAccess("N4", 3)
  birth_date = ElementAccess("DMG", 2, x12type=D8)
  gender = ElementAccess("DMG", 3, x12type=enum({
      "M": "Male",
      "F": "Female",
      "U": "Unknown"}))
  marital_status = ElementAccess("DMG", 4, x12type=enum({
      "M": "Married",
      "S": "Single"}))

  def __init__(self, aLoop, *args, **kwargs):
    super(Member, self).__init__(aLoop, *args, **kwargs)


class Coverage(BaseLoop):
  """Health Coverage information from the 2300 loop."""

  loopName = "2300"

  insurance_line_code = ElementAccess("HD", 3, x12type=enum({
      "HLT": "Generic Medical",
      "DEN": "Generic Dental",
      "VIS": "Generic Vision",
      "MM": "MM",
      "PPO": "Preferred Provider Organization",
      "POS": "Point of Service",
      "HMO": "Health Maintenance Organization",
      "DCP": "Dental Capitation"}))
  coverage_description = ElementAccess("HD", 4)
  coverage_level_code = ElementAccess("HD", 5)
  benefit_begin_date = ElementAccess("DTP", 3, x12type=D8)
  plan_code = ElementAccess("REF", 2)
  plan_account_number = None
  plan_branch = None
  plan_benefit_options = None
  plan_network = None

  def __init__(self, aLoop, *args, **kwargs):
    super(Coverage, self).__init__(aLoop, *args, **kwargs)
    if self.plan_code:
      self.plan_account_number = self.plan_code[:7]
      self.plan_branch = self.plan_code[7:12]
      self.plan_benefit_options = self.plan_code[12:16]
      self.plan_network = self.plan_code[16:]


class Provider(BaseLoop):
  """Provider information from the 2310 loop."""

  loopName = "2310"

  pcp_code = ElementAccess("NM1", 9)

  def __init__(self, aLoop, *args, **kwargs):
    super(Provider, self).__init__(aLoop, *args, **kwargs)


class Subscriber(object):
  def __init__(self, member_level, member, coverage, provider):
    self.member_level = member_level
    self.member = member
    self.coverage = coverage
    self.provider = provider

  def __str__(self):
    return json.dumps(_to_python_dict(self), indent=2)


class F834_5010(Facade):
  transaction_set_identifier_code = '834'
  x12_version_string = '5010'

  def __init__(self, anX12Message):
    """Examine the message and extract the relevant Loops."""
    st_loops = anX12Message.descendant('LOOP', name='ST_LOOP')
    if st_loops:
      self.facades = [F834_5010(loop) for loop in st_loops]
    else:
      self.sponsor = first(self.loops(Sponsor, anX12Message))
      self.payer = first(self.loops(Payer, anX12Message))
      self.member_levels = self.loops(MemberLevel, anX12Message)
      self.members = self.loops(Member, anX12Message)
      self.coverages = self.loops(Coverage, anX12Message)
      self.providers = self.loops(Provider, anX12Message)
      self.subscribers = []
      for i in range(len(self.member_levels)):
        subscriber = Subscriber(
          self.member_levels[i],
          self.members[i] if len(self.members) > i else None,
          self.coverages[i] if len(self.coverages) > i else None,
          self.providers[i] if len(self.providers) > i else None)
        self.subscribers.append(subscriber)