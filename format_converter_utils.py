import os

from common.data.mysql_utils import MySqlUtils
from common.build.pgp_utils import PgpUtils
from common.send.aws_utils import AwsUtils
from credentials import (local_output_directory,
                         get_pgp_key_to_decrypt,
                         pgp_passphrase,
                         pgp_encrypted_file_extension,
                         prod_db_endpoint as db_endpoint,
                         prod_db_username as db_username,
                         prod_db_password as db_password,
                         prod_db_name as db_name,
                         aws_region_name,
                         aws_access_key_id,
                         aws_secret_access_key,)

def format_date(date):
  return date.strftime('%Y%m%d') if date and not str(date).startswith('0000') else None

def get_date_filter_from_parameter(date_filter):
  operator = None
  date = None
  if date_filter:
    if date_filter[0].isdigit():
      operator = '='
      date = date_filter
    else:
      if date_filter[1].isdigit():
        operator = date_filter[0]
        date = date_filter[1:]
      else:
        operator = date_filter[0:2]
        date = date_filter[2:]
  return f"{operator} '{date}'" if operator else None

def get_db_loader():
  return MySqlUtils(db_endpoint, db_username, db_password, db_name)

def write_to_s3(output_generator, aws_s3_bucket_name, file_name):
  output_generator.write_output_to_bucket(file_name, aws_s3_bucket_name, aws_region_name, aws_access_key_id, aws_secret_access_key)
  print(f'Uploaded file "{file_name}" to bucket "{aws_s3_bucket_name}"')

def write_to_local(output_generator, aws_s3_bucket_name, file_path):
  file_path = os.path.abspath(os.path.join(local_output_directory, aws_s3_bucket_name, file_path).replace('\\', os.sep).replace('/', os.sep))
  directory = os.path.dirname(file_path)
  if not os.path.exists(directory):
    os.makedirs(directory)
  output_generator.write_output_to_file(file_path)
  print(f'Generated local copy in "{file_path}"')

def sync_s3_with_local(aws_s3_bucket_name):
  output_directory = os.path.abspath(os.path.join(local_output_directory, aws_s3_bucket_name).replace('\\', os.sep).replace('/', os.sep))
  print(f'Starting to sync bucket "{aws_s3_bucket_name}" to local directory "{output_directory}""')
  aws_utils = AwsUtils(aws_region_name, aws_access_key_id, aws_secret_access_key)
  aws_utils.download_s3_dir(aws_s3_bucket_name, '', output_directory)
  print('Sync completed successfully.')

def decrypt_files(input_file_list, output_dir, show_output=True, show_pgp_output=False, exit_on_failed=True):
  private_key_path = get_pgp_key_to_decrypt(show_output, exit_on_failed)
  pgp_utils = PgpUtils(private_key_path=private_key_path, passphrase=pgp_passphrase, encrypted_file_extension=pgp_encrypted_file_extension)
  decrypted_file_list = pgp_utils.decrypt_files(input_file_list, output_dir, show_output, show_pgp_output)
  os.remove(private_key_path)
  return decrypted_file_list
