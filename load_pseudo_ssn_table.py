import sys
import re
from datetime import datetime

from common.data.mysql_utils import MySqlUtils
from credentials import (prod_db_endpoint as db_endpoint,
                         prod_db_username as db_username,
                         prod_db_password as db_password,
                         prod_db_name as db_name)

# required import info
db_table_name = 'pseudo_ssn'

def __validate_input():
  def print_error_and_exit():
    print('Please input the 9-digit ssn start and end ranges in order in xxx-xx-xxxx format.\nUsage: python load_pseudo_ssn_table.py 123-45-6789 123-45-9876')
    sys.exit(0)

  if len(sys.argv) < 3:
    print('Error: Insufficient arguments passed!')
    print_error_and_exit()

  start = sys.argv[1]
  end = sys.argv[2]

  ssn_pattern = r'^\d{3}-\d{2}-\d{4}$'
  if re.fullmatch(ssn_pattern, start) is None:
    print('Error: Start range is invalid!')
    print_error_and_exit()
  if re.fullmatch(ssn_pattern, end) is None:
    print('Error: End range is invalid!')
    print_error_and_exit()

  start = int(start.replace('-', ''))
  end = int(end.replace('-', ''))
  if end <= start:
    print('Error: End range should be greater than the start range!')
    print_error_and_exit()

  return (start, end)

def __generate_data_dict_list(start_ssn, end_ssn):
  data_dict_list = []
  while start_ssn <= end_ssn:
    data_dict = {
      'ssn': start_ssn,
      'load_date': datetime.utcnow()
    }
    data_dict_list.append(data_dict)
    start_ssn += 1
  return data_dict_list

def run_insertion(start_ssn, end_ssn):
  data_dict_list = __generate_data_dict_list(start_ssn, end_ssn)
  db_loader = MySqlUtils(db_endpoint, db_username, db_password, db_name)
  invalid_field_names, num_of_rows_inserted = db_loader.insert_data_dict_list_to_db(db_table_name, data_dict_list)

  if invalid_field_names:
    print('Invalid field names in data list:', ', '.join(invalid_field_names))
  print(f'{num_of_rows_inserted} records inserted successfully!')

if __name__ == '__main__':
  start_ssn, end_ssn = __validate_input()
  run_insertion(start_ssn, end_ssn)