import sys
import argparse
import re
from datetime import datetime

from common.data.mysql_utils import MySqlUtils
from common.build.data_field import DataField
from common.build.csv_generator import CsvGenerator
from credentials import (prod_db_endpoint as db_endpoint,
                         prod_db_username as db_username,
                         prod_db_password as db_password,
                         prod_db_name as db_name,
                         aws_region_name,
                         aws_access_key_id,
                         aws_secret_access_key)

# aws bucket
aws_s3_bucket_name = 'benzer.benebay-carevalet'

def __validate_input():
  def validate_start_number(val):
    if not re.match(r'.*\d{6}$', val):
      raise argparse.ArgumentTypeError
    return val

  parser = argparse.ArgumentParser(description='Create member excel file from eligibility_load_history table.')
  parser.add_argument('--start-from-member', dest='start_from_member', required=True, type=validate_start_number, help='The start value for member ids; any string ending with 6 digits (ex. M8000000005)')
  parser.add_argument('--from-batch', dest='from_batch', required=False, help='Start batch to copy from (defaults to 00000)', default='00000')
  parser.add_argument('--to-batch', dest='to_batch', required=False, help='End batch to copy from (defaults to 99999)', default='99999')
  parser.add_argument('--create-local-output', dest='create_local_output', action='store_true', help='If provided, creates the local output')

  try:
    args = parser.parse_args()
    return args.start_from_member, args.from_batch, args.to_batch, args.create_local_output
  except:
    parser.print_help()
    sys.exit(0)

# date formatter used for mapping
def __format_date(date):
  return date.strftime('%Y%m%d') if date and not str(date).startswith('0000') else None

# get data from mysql db
def __get_db_data(start_from_member, from_batch, to_batch):
  db_loader = MySqlUtils(db_endpoint, db_username, db_password, db_name)
  from_batch = from_batch.zfill(5)
  to_batch = to_batch.zfill(5)
  query = f'''
    SELECT *
    FROM eligibility_load_history
    WHERE
      batch_id BETWEEN '{from_batch}' AND '{to_batch}'
  '''
  db_dict_list = db_loader.fetch_all_from_query(query)

  start_from_member_prefix = start_from_member[:-6]
  start_from_member_number = int(start_from_member[-6:])
  benefit_plan_name_map = {'BRONZE': '8001BRONZE', 'SILVER': '8002SILVER', 'GOLD': '8003GOLD'}
  for record in sorted(db_dict_list, key=lambda record: record['relationship_code']):
    is_primary, is_spouse = record['relationship_code'] == '01', record['relationship_code'] == '02'
    record['member_ssn'] = record['subscriber_ssn'] if is_primary else record['spouse_ssn'] if is_spouse else record['dependent_ssn']
    record['member_id'] = start_from_member_prefix + str(start_from_member_number).zfill(6)
    record['last_name'] = record['last_name'] if is_primary else record['spouse_last_name'] if is_spouse else record['dependent_last_name']
    record['middle_name'] = record['middle_name'] if is_primary else record['spouse_middle_name'] if is_spouse else record['dependent_middle_name']
    record['first_name'] = record['first_name'] if is_primary else record['spouse_first_name'] if is_spouse else record['dependent_first_name']
    record['birthday'] = record['birth_date'] if is_primary else record['spouse_birth_date'] if is_spouse else record['dependent_birth_date']
    benefit = benefit_plan_name_map.get((record['benefit_plan_name'] or '').upper())
    if benefit:
      record['benefit_plan_name'] = record['benefit_plan_code'] = benefit
    if not is_primary:
      parent = list(filter(lambda row: record['employee_id'] == row['employee_id'] and record['batch_id'] == row['batch_id'] and row['relationship_code'] == '01', db_dict_list)) \
               or list(filter(lambda row: record['employee_id'] == row['employee_id'] and row['relationship_code'] == '01', db_dict_list))
      parent = parent[0] if parent else None
      record['address'] = parent['address'] if parent else ''
      record['city'] = parent['city'] if parent else ''
      record['county'] = parent['county'] if parent else ''
      record['state'] = parent['state'] if parent else ''
      record['zip_code'] = parent['zip_code'] if parent else ''
      record['subscriber_ssn'] = parent['subscriber_ssn']
      record['subscriber_member_id'] = parent['subscriber_member_id']
    else:
      record['subscriber_member_id'] = record['member_id']
    start_from_member_number += 1

  return db_dict_list

# build converted data
def __build_data(db_dict_list):
  data_fields = [
    DataField('subscriber_ssn', 'Subscriber SSN'),
    DataField('subscriber_member_id', 'Subscriber Member ID'),
    DataField(None, 'Group Number', default_val='Benzer Default'),
    DataField('group_number', 'Sub-Group Number'),
    DataField('last_name', 'Last Name'),
    DataField('first_name', 'First Name'),
    DataField('middle_name', 'Middle Name'),
    DataField('address', 'Address'),
    DataField('address_line_2', 'Address Line 2'),
    DataField('city', 'City'),
    DataField('county', 'County'),
    DataField('state', 'State'),
    DataField('zip_code', 'Zip Code'),
    DataField('phone', 'Phone'),
    DataField('gender', 'Gender'),
    DataField('member_ssn', 'Member SSN'),
    DataField('member_id', 'Member ID'),
    DataField('relationship_code', 'Relationship Code'),
    DataField('birthday', 'Birth Date', __format_date),
    DataField('benefit_plan_code', 'Benefit Plan Code'),
    DataField('benefit_plan_name', 'Benefit Plan Name'),
    DataField('enrollment_status', 'Enrollment Status'),
    DataField('enrollment_date', 'Enrollment Date', __format_date),
    DataField('eligibility_start_date', 'Eligibility Start Date', __format_date),
    DataField('eligibility_end_date', 'Eligibility End Date', __format_date),
    DataField('disenroll_date', 'Disenroll Date', __format_date),
    DataField(None, 'PCP', default_val='Default Provider'), #'pcp'
    DataField('pcp_begin_date', 'PCP Begin Date', __format_date),
    DataField('pcp_end_date', 'PCP End Date', __format_date),
    DataField(None, 'Region', default_val='Default Service Region'), #'region'
    DataField(None, 'Region Begin Date', __format_date),
    DataField(None, 'Region End Date', __format_date),
    DataField('insurance', 'Insurance'),
    DataField('insurance_begin_date', 'Insurance Begin Date', __format_date),
    DataField('insurance_end_date', 'Insurance End Date', __format_date),
    DataField('disenroll_code', 'Disenroll Code'),
    DataField('medicare_number', 'Medicare Number'),
    DataField('employment_date', 'Employment Date', __format_date),
    DataField('member_email_address', 'Member Email Address'),
    DataField('employee_id', 'Employee ID'),
    DataField('previous_subscriber_ssn', 'Previous Subscriber SSN'),
    DataField('previous_last_name', 'Previous Last Name'),
    DataField('previous_first_name', 'Previous First Name'),
    DataField('previous_gender', 'Previous Gender'),
    DataField('previous_relationship_code', 'Previous Relationship Code'),
    DataField('previous_birth_date', 'Previous Birth Date', __format_date),
  ]
  return CsvGenerator(db_dict_list, data_fields, save_as_excel=True)

# send converted data to s3 bucket and sftp server
def __send_converted_data(output_generator, create_local_output=False):
  file_name = f'uzio_member_{datetime.utcnow().strftime("%Y%m%d_%H%M%S")}.xlsx' # format: uzio_member_CCYYMMDD_HHMMSS.xlsx
  output_generator.write_output_to_bucket(f'outbox/{file_name}', aws_s3_bucket_name, aws_region_name, aws_access_key_id, aws_secret_access_key, write_header=True)
  if create_local_output:
    output_generator.write_output_to_file(file_name, write_header=True)

def run_conversion(start_from_member, from_batch, to_batch, create_local_output):
  db_dict_list = __get_db_data(start_from_member, from_batch, to_batch)
  output_generator = __build_data(db_dict_list)
  __send_converted_data(output_generator, create_local_output)

if __name__ == '__main__':
  start_from_member, from_batch, to_batch, create_local_output = __validate_input()
  run_conversion(start_from_member, from_batch, to_batch, create_local_output)