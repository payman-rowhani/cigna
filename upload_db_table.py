import sys
import argparse
import traceback

from common.data.csv_utils import CsvUtils
from credentials import db_alias_to_credentials_map, get_db_loader

def __validate_input():
  parser = argparse.ArgumentParser(description='Load data from a cvs/excel file to a database table.')
  parser.add_argument('--source-file', dest='source_file', required=True, help=f'Source csv/excel file path')
  parser.add_argument('--csv-delimiter', dest='csv_delimiter', required=False, help=f'Csv delimiter used in source file (defaults to comma; does not apply to excel file)', default=',')
  parser.add_argument('--read-record-buffer', dest='read_record_buffer', type=int, required=False, help=f'Number of rows to read from the csv file in each iteration (defaults to all rows altogether)', default=0)
  parser.add_argument('--max-record-count', dest='max_record_count', type=int, required=False, help=f'Maxmimum number of rows insert in db (defaults to all rows)', default=0)
  parser.add_argument('--source-date-format', dest='source_date_format', required=False, help=f'Source date format to parse', default='MM/DD/YYYY')
  parser.add_argument('--insert-batch-size', dest='insert_batch_size', type=int, required=False, help=f'Number of records to upload and insert to db in each iteration (defaults to 1000)', default=1000)
  parser.add_argument('--target-db', dest='target_db_alias', required=True, choices=db_alias_to_credentials_map.keys(), help=f'Target db alias name')
  parser.add_argument('--target-table', dest='target_table_name', required=True, help='Target db table name')
  parser.add_argument('--resume-insertion', dest='resume_insertion', action='store_true', help='If provided, the insertion process will continue based on the number of current records in database')

  try:
    args = parser.parse_args()
    return (args.source_file,
            args.csv_delimiter.strip('"').strip("'"),
            args.read_record_buffer,
            args.max_record_count,
            args.source_date_format.replace('YYYY', '%Y').replace('MM', '%m').replace('DD', '%d'),
            args.insert_batch_size,
            args.target_db_alias,
            args.target_table_name,
            args.resume_insertion)
  except:
    parser.print_help()
    sys.exit(0)

def __refine_ppo_data(data_dict_list, db_loader):
  for item in data_dict_list:
    if 'irs_holdback_flag' in item:
      if item['irs_holdback_flag'] == 'INACTIVE':
        item['irs_holdback_flag'] = 'I'
      elif item['irs_holdback_flag'] == 'ACTIVE':
        item['irs_holdback_flag'] = 'A'

    if 'provider_ent_type' in item:
      if item['provider_ent_type'] == 'FACILITY':
        item['provider_ent_type'] = 'F'
      elif item['provider_ent_type'] == 'GROUP':
        item['provider_ent_type'] = 'G'

    if 'addess_county' in item:
      item['address_county'] = item['addess_county']
      del item['addess_county']

def __get_source_file_data_batches(source_file, csv_delimiter, source_date_format, insert_batch_size, max_rows_count, row_offset, db_loader, refine_data_function, field_names_map):
  is_excel = source_file.endswith('.xlsx') or source_file.endswith('.xls')
  data_dict_list = CsvUtils(source_file, is_excel=is_excel).get_data_dict_list(field_names_map=field_names_map, delimiter=csv_delimiter, date_fields_format=source_date_format, max_rows_count=max_rows_count, row_offset=row_offset)
  if refine_data_function:
    refine_data_function(data_dict_list, db_loader)
  data_dict_batches = []
  for i in range(0, len(data_dict_list), insert_batch_size):
    data_dict_batches.append(data_dict_list[:insert_batch_size])
    del data_dict_list[:insert_batch_size]
  return data_dict_batches

def __copy_to_target_table(db_loader, target_table_name, source_file_data_batches, current_batch_num=1, current_total_records_inserted=0):
  try:
    db_loader.fetch_all_from_query(f'SELECT 1 from {target_table_name}')
  except Exception as e:
    print(f'Error: Invalid target table \'{target_table_name}\' passed!')
    print('Error details:', e)
    traceback.print_exc()
    sys.exit(0)

  invalid_field_names = ''
  total_records_inserted = 0
  batch_num = current_batch_num
  for data_batch in source_file_data_batches:
    if not data_batch:
      break
    invalid_field_names, num_of_rows_inserted = db_loader.insert_data_dict_list_to_db(target_table_name, data_batch, nullify_empty_values=True)

    if total_records_inserted == 0 and invalid_field_names:
      print('Warning: Invalid field names in source table:', ', '.join(invalid_field_names))

    total_records_inserted += num_of_rows_inserted
    print(f'Batch {batch_num} inserted {num_of_rows_inserted} records (for total of {total_records_inserted + current_total_records_inserted}).')
    batch_num += 1

  return invalid_field_names, total_records_inserted, batch_num

def run_insertion(source_file, csv_delimiter, read_record_buffer, max_record_count, source_date_format, insert_batch_size, target_db_alias, target_table_name, resume_insertion, refine_data_function=None, field_names_map=None, pre_insert_function=None):
  invalid_field_names = ''
  total_num_of_rows_inserted = 0
  current_batch_num = 1
  if max_record_count:
    read_record_buffer = min(read_record_buffer, max_record_count)

  db_loader = get_db_loader(target_db_alias)

  if pre_insert_function:
    pre_insert_function(db_loader)

  if resume_insertion:
    print('Checking previously added records in db...')
    total_num_of_rows_inserted = db_loader.fetch_all_from_query(f'SELECT count(*) AS total FROM {target_table_name}')[0]['total']
    print(f'Found {total_num_of_rows_inserted if total_num_of_rows_inserted else "no"} existing records.')

  round_counter = 1
  while True:
    if max_record_count and total_num_of_rows_inserted + read_record_buffer > max_record_count:
      read_record_buffer = max_record_count - total_num_of_rows_inserted

    print(f'Loading data from {source_file} (round {round_counter})...')
    source_file_data_batches = __get_source_file_data_batches(source_file, csv_delimiter, source_date_format, insert_batch_size, max_rows_count=read_record_buffer, row_offset=total_num_of_rows_inserted, db_loader=db_loader, refine_data_function=refine_data_function, field_names_map=field_names_map)
    if not source_file_data_batches or not source_file_data_batches[0]:
      print('No more records to insert into db!')
      break

    print(f'Starting insertion process (round {round_counter})...')
    invalid_field_names, num_of_rows_inserted, batch_num = __copy_to_target_table(db_loader, target_table_name, source_file_data_batches, current_batch_num, total_num_of_rows_inserted)
    current_batch_num = batch_num
    total_num_of_rows_inserted += num_of_rows_inserted

    if max_record_count and total_num_of_rows_inserted >= max_record_count:
      print('No more records to insert into db!')
      break

    if len(source_file_data_batches[-1]) < insert_batch_size:
      print('No more records to insert into db!')
      break

    round_counter += 1

  db_loader.close_db_connection()
  print(f'Total number of {total_num_of_rows_inserted} records inserted successfully!')
  if invalid_field_names:
    print('Warning: Invalid field names in source table:', ', '.join(invalid_field_names))

if __name__ == '__main__':
  source_file, csv_delimiter, read_record_buffer, max_record_count, source_date_format, insert_batch_size, target_db_alias, target_table_name, resume_insertion = __validate_input()
  run_insertion(source_file, csv_delimiter, read_record_buffer, max_record_count, source_date_format, insert_batch_size, target_db_alias, target_table_name, resume_insertion, __refine_ppo_data)