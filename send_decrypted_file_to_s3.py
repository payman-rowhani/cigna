import sys
import os
import traceback
import argparse
import tempfile
import shutil

from common.send.aws_utils import AwsUtils
from common.send.sftp_utils import SftpUtils
from format_converter_utils import decrypt_files
from credentials import (local_output_directory,
                         get_aws_utils,
                         get_ftp_credentials,
                         pgp_encrypted_file_extension,
                         vendor_to_bucket_map)

vendor_to_bucket_directory_map = {
  'cigna': 'claims/unencrypted',
  'sgrx': 'download',
  'carevalet': 'download'
}
vendor_to_local_directory_map = {
  'cigna': 'unencrypted',
  'sgrx': 'benzer.benebay-sgrx/download',
  'carevalet': 'benzer.benebay-carevalet/download'
}

def __validate_input():
  parser = argparse.ArgumentParser(description='Decrypt files from ftp sites and send to s3 buckets.')
  parser.add_argument('--vendor', dest='vendor', required=False, choices=vendor_to_bucket_map.keys(), help=f'Vendor name', default='cigna')

  try:
    args = parser.parse_args()
    return args.vendor
  except:
    parser.print_help()
    sys.exit(0)

def __get_bucket_name(vendor):
  return vendor_to_bucket_map[vendor]

def __get_local_output_directory(extract_dirs_from_ftp, ftp_directory, vendor):
  if extract_dirs_from_ftp:
    return os.path.join(local_output_directory, __get_bucket_name(vendor), ftp_directory.lstrip('/'))
  else:
    return os.path.join(local_output_directory, vendor_to_local_directory_map[vendor])

def __get_upload_directory(extract_dirs_from_ftp, ftp_directory, vendor):
  if extract_dirs_from_ftp:
    return ftp_directory.lstrip('/')
  else:
    return vendor_to_bucket_directory_map[vendor]

def __get_files_to_decrypt(should_decrypt, bucket_name, local_dir, upload_dir, ftp_credentials, ftp_utils):
  print(f'Downloading new files from ftp server "{ftp_credentials["address"]}:{ftp_utils.port}" and directory "{ftp_credentials["directory"]}"')

  aws_utils = get_aws_utils()
  def skip_file_if_already_decrypted(file_path):
    new_file_path = file_path
    if should_decrypt and new_file_path.lower().endswith(pgp_encrypted_file_extension):
      new_file_path = os.path.splitext(new_file_path)[0]
    new_file_path = os.path.basename(new_file_path)
    file_name_on_bucket = f'{upload_dir}/{new_file_path}'
    file_name_on_local = f'{local_dir}/{new_file_path}'
    return os.path.exists(file_name_on_local) and aws_utils.s3_file_exists(bucket_name, file_name_on_bucket)

  temp_dir = tempfile.mkdtemp() if should_decrypt else local_dir
  downloaded_file_list = ftp_utils.download_all_files(ftp_credentials['directory'], temp_dir, pgp_encrypted_file_extension if should_decrypt else '', skip_file_function=skip_file_if_already_decrypted, show_output=True)
  return temp_dir, downloaded_file_list

def __upload_file_to_s3(should_decrypt, bucket_name, upload_dir, file_list):
  aws_utils = get_aws_utils()
  for file_path in file_list:
    print(f'Uploading {"decyrpted" if should_decrypt else "pulled"} file "{os.path.basename(file_path)}" to s3 bucket "{bucket_name}" and directory "{upload_dir}"')
    file_name_on_bucket = f'{upload_dir}/{os.path.basename(file_path)}'
    aws_utils.upload_file_to_s3(bucket_name, file_path, file_name_on_bucket, error_if_duplicate_key=False)

def run_send(vendor, should_decrypt=True, extract_dirs_from_ftp=False, is_claims_credential=True, sftp_port=22):
  ftp_credentials, ftp_utils = get_ftp_credentials(vendor, is_claims_credential, sftp_port)
  bucket_name = __get_bucket_name(vendor)
  upload_dir = __get_upload_directory(extract_dirs_from_ftp, ftp_credentials['directory'], vendor)
  local_dir = __get_local_output_directory(extract_dirs_from_ftp, ftp_credentials['directory'], vendor)
  if not os.path.exists(local_dir):
    os.makedirs(local_dir)

  download_temp_dir, ftp_file_list = __get_files_to_decrypt(should_decrypt, bucket_name, local_dir, upload_dir, ftp_credentials, ftp_utils)
  if not ftp_file_list:
    if should_decrypt:
      verbose = "You're already all set! No new encrypted file found to download, decrypt, and upload to s3."
    else:
      verbose = "You're already all set! No new ftp file found to upload to s3."
    print(verbose)
  else:
    if should_decrypt:
      descrypted_file_list = decrypt_files(ftp_file_list, local_dir)
      __upload_file_to_s3(should_decrypt, bucket_name, upload_dir, descrypted_file_list)
    else:
      __upload_file_to_s3(should_decrypt, bucket_name, upload_dir, ftp_file_list)

  if should_decrypt and download_temp_dir != local_dir:
    shutil.rmtree(download_temp_dir)

if __name__ == '__main__':
  import warnings
  warnings.filterwarnings('ignore')

  vendor = __validate_input()
  run_send(vendor)