import sys
import os
import argparse

from common.data.csv_utils import CsvUtils

def __validate_input():
  parser = argparse.ArgumentParser(description='Split input excel file to multiple based on number of states included.')
  parser.add_argument('--input', dest='input', required=True, help=f'Input excel/csv file name')
  parser.add_argument('--output-dir', dest='output_dir', required=False, help=f'Output directory for split files (defaults to current directory)', default='.')
  parser.add_argument('--states-count', dest='states_count', type=int, choices=range(1, 20), required=False, help='Number of states per file (defaults to 3)', default=3)

  try:
    args = parser.parse_args()
    return args.input, args.output_dir, args.states_count
  except:
    parser.print_help()
    sys.exit(0)

class StateData:
  def __init__(self, states, output_dir, dict_list):
    self.states = states
    self.output_dir = output_dir
    self.dict_list = dict_list

  def generate_output(self):
    if not os.path.exists(self.output_dir):
      os.makedirs(self.output_dir)

    output_file_name = f'zip_codes_{"_".join(self.states)}.xlsx'
    output_file_name = os.path.join(self.output_dir, output_file_name)
    CsvUtils(output_file_name, is_excel=True).write_data_dict_list(self.dict_list, write_header=True)
    print('Generated', output_file_name, f'({len(self.dict_list)} rows)')

def split_data(input, output_dir, states_count):
  # get and split states to chunks
  data_dict_list = CsvUtils(input, is_excel=True).get_data_dict_list()
  all_states = sorted(list(set([item['state'].strip() for item in data_dict_list])))
  state_chunks = [all_states[i:i + states_count] for i in range(0, len(all_states), states_count)]

  # generate output for each state chunk
  for state_chunk in state_chunks:
    dict_list = []
    for state in state_chunk:
      state_list = filter(lambda item: item['state'].strip() == state, data_dict_list)
      dict_list.extend(state_list)
    StateData(state_chunk, output_dir, dict_list).generate_output()

  return len(state_chunks)

if __name__ == '__main__':
  input, output_dir, states_count = __validate_input()
  state_chunks_count = split_data(input, output_dir, states_count)
  print(state_chunks_count, 'files generated in', output_dir if output_dir != '.' else 'current directory')
