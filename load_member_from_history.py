import sys
import argparse
from datetime import datetime

from credentials import db_alias_to_credentials_map, get_db_loader

__history_table_name = 'eligibility_load_history'

def __validate_input():
  parser = argparse.ArgumentParser(description='Loads member table from eligibility_load_history table.')
  parser.add_argument('--db', dest='db_alias', required=False, choices=db_alias_to_credentials_map.keys(), help=f'db alias name', default='PROD')
  parser.add_argument('--from-batch', dest='from_batch', required=False, help='Start batch to copy from (defaults to 00000)', default='00000')
  parser.add_argument('--to-batch', dest='to_batch', required=False, help='End batch to copy from (defaults to 99999)', default='99999')

  try:
    args = parser.parse_args()
    return args.db_alias, args.from_batch, args.to_batch
  except:
    parser.print_help()
    sys.exit(0)

def __get_history_table_data(db_loader, from_batch, to_batch):
  from_batch = from_batch.zfill(5)
  to_batch = to_batch.zfill(5)
  query = f'''
    SELECT *
    FROM {__history_table_name}
    WHERE
      batch_id BETWEEN '{from_batch}' AND '{to_batch}'
  '''
  history_table_data = db_loader.fetch_all_from_query(query)
  return history_table_data

def __get_member_table_data(db_loader, history_table_data):
  next_member_id = (db_loader.fetch_all_from_query('SELECT MAX(id) AS max_id FROM member')[0]['max_id'] or 0) + 1
  members = []
  for item in sorted(history_table_data, key=lambda record: record['relationship_code']):
    parent = None
    if item['relationship_code'] != '01':
      parent = list(filter(lambda row: item['employee_id'] == row['employee_id'] and item['batch_id'] == row['batch_id'] and row['relationship_code'] == '01', history_table_data))
      if not parent:
        parent = list(filter(lambda row: item['employee_id'] == row['employee_id'] and row['relationship_code'] == '01', history_table_data))
      parent = parent[0] if parent else None
    item['inserted_member_id'] = next_member_id
    member = {
      'id': item['inserted_member_id'],
      'last_name': (item['last_name'] if item['relationship_code'] == '01' else item['spouse_last_name'] if item['relationship_code'] == '02' else item['dependent_last_name']) or '',
      'middle_name': (item['middle_name'] if item['relationship_code'] == '01' else item['spouse_middle_name'] if item['relationship_code'] == '02' else item['dependent_middle_name']) or '',
      'first_name': (item['first_name'] if item['relationship_code'] == '01' else item['spouse_first_name'] if item['relationship_code'] == '02' else item['dependent_first_name']) or '',
      'sex': {'M': 'male', 'F': 'female'}.get((item['gender'] or '').upper()),
      'birthday': (item['birth_date'] if item['relationship_code'] == '01' else item['spouse_birth_date'] if item['relationship_code'] == '02' else item['dependent_birth_date']) or '',
      'address': parent['address'] if parent else item['address'],
      'city': parent['city'] if parent else item['city'],
      'county': parent['county'] if parent else item['county'],
      'state': parent['state'] if parent else item['state'],
      'zip_code': parent['zip_code'] if parent else item['zip_code'] or '',
      'phone': item['phone'] or '',
      'email': item['member_email_address'] or '',
      'data_of_hire': item['employment_date'],
      'benefit_plan_id': {'BRONZE': '2', 'SILVER': '3', 'GOLD': '4'}.get((item['benefit_plan_name'] or '').upper()),
      'relation_type': {'01': None, '02': 'spouse', '03': 'child'}.get(item['relationship_code']),
      'eligibility_from': item['eligibility_start_date'],
      'eligibility_till': item['eligibility_end_date'],
      'has_another_insurance': {'Y': '1', 'N': '0'}.get((item['other_insurance'] or '').upper(), '0'),
      'group_number': item['group_number'] or '',
      'pcp_begin_date': item['pcp_begin_date'],
      'pcp_end_date': item['pcp_end_date'],
      'disenroll_date': item['disenroll_date'],
      'insurance': item['insurance'] or '',
      'insurance_begin_date': item['insurance_begin_date'],
      'insurance_end_date': item['insurance_end_date'],
      'member_id': item['employee_id'] or '0',
      'level_id': '0',
      'home_phone': '',
      'work_phone': '',
      'company_id': '1',
      'language_id': '0',
      'service_region_id': '0',
      'status': 'disenrolled' if item['disenroll_date'] else 'enrolled' if (not item['eligibility_end_date'] or item['eligibility_end_date'] >= datetime.utcnow().date()) else 'terminated',
      'relation_id': '0' if item['relationship_code'] == '01' else parent['inserted_member_id'] if parent else '0',
      'code': '',
      'person_id': item['inserted_member_id'],
      'primary_care_provider_id': '0',
      'disenroll_reason_id': '0',
      'verified': '0',
      'source': 'manual',
      'status_change': 'none',
      'is_deleted': '0',
      'sub_group_number': 'none',
      'instance': '0',
      'benefit_plan_date': '0000-00-00 00:00:00',
      'status_data': 'created',
      'status_filling': 'partial',
      'status_eligible': '0',
      'monthly_premium_fee': '0',
      'terminate_reason_id': '0',
      'enrolled_by': '0',
      'disenrolled_by': '0',
      'terminated_by': '0',
      'published_by': '0',
      'cob_active': '1',
      'comment': ''
    }
    members.append(member)
    next_member_id += 1
  return members, history_table_data

def __get_member_external_keys_table_data(history_member_table_data):
  member_external_keys = []
  for item in history_member_table_data:
    ssn = None
    if item['subscriber_ssn'] and item['relationship_code'] == '01':
      ssn = item['subscriber_ssn']
    elif item['spouse_ssn'] and item['relationship_code'] == '02':
      ssn = item['spouse_ssn']
    elif item['dependent_ssn'] and item['relationship_code'] == '03':
      ssn = item['dependent_ssn']
    if ssn:
      member_external_key = {
        'company_id': '1',
        'member_id': item['inserted_member_id'],
        'external_key_id': '3',
        'value': ssn,
        'is_deleted': False
      }
      member_external_keys.append(member_external_key)

    if item['medicare_number']:
      member_external_key = {
        'company_id': '1',
        'member_id': item['inserted_member_id'],
        'external_key_id': '1',
        'value': item['medicare_number'],
        'is_deleted': False
      }
      member_external_keys.append(member_external_key)
  return member_external_keys

def __get_member_fee_table_data(history_member_table_data):
  member_fees = []
  for item in history_member_table_data:
    member_fee = {
      'member_id': item['inserted_member_id'],
      'month': '1',
      'year': '2019',
      'payed': True,
      'delay': None,
      'is_deleted': False,
      'payed_amount': None,
      'approved_delay': False
    }
    member_fees.append(member_fee)
  return member_fees

def __copy_to_target_table(db_loader, target_table_name, table_data):
  num_of_rows_inserted = db_loader.insert_data_dict_list_to_db(target_table_name, table_data, nullify_empty_values=False)[-1]
  return num_of_rows_inserted

def run_insertion(db_alias, from_batch, to_batch):
  db_loader = get_db_loader(db_alias)
  history_table_data = __get_history_table_data(db_loader, from_batch, to_batch)

  member_table_data, history_member_table_data = __get_member_table_data(db_loader, history_table_data)
  num_of_members_inserted = __copy_to_target_table(db_loader, 'member', member_table_data)
  print(f'{num_of_members_inserted} records inserted to "member" table successfully!')

  member_external_keys_table_data = __get_member_external_keys_table_data(history_member_table_data)
  if member_external_keys_table_data:
    num_of_member_external_keys_inserted = __copy_to_target_table(db_loader, 'member_external_keys', member_external_keys_table_data)
    print(f'{num_of_member_external_keys_inserted} records inserted to "member_external_keys" table successfully!')

  # member_fee_table_data = __get_member_fee_table_data(history_member_table_data)
  # num_of_member_fees_inserted = __copy_to_target_table(db_loader, 'member_fee', member_fee_table_data)
  # print(f'{num_of_member_fees_inserted} records inserted to "member_fee" table successfully!')

  db_loader.close_db_connection()

if __name__ == '__main__':
  db_alias, from_batch, to_batch = __validate_input()
  run_insertion(db_alias, from_batch, to_batch)