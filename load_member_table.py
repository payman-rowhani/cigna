from common.data.csv_utils import CsvUtils
from common.data.mysql_utils import MySqlUtils
from credentials import (prod_db_endpoint as db_endpoint,
                         prod_db_username as db_username,
                         prod_db_password as db_password,
                         prod_db_name as db_name)

# required import info
excel_file = 'load_member_data.xlsx'
db_table_name = 'member'
foreign_columns_to_fix = [
  ('benefit_plan', 'benefit_plan_id'),
  ('provider', 'primary_care_provider_id')
]

def __fix_foreign_columns(foreign_table, foreign_column, db_loader, data_dict_list):
  foreign_table_ids = [item['id'] for item in db_loader.fetch_all_from_table(foreign_table)]
  default_foreign_id = foreign_table_ids[0] if foreign_table_ids else None
  for item in data_dict_list:
    if int(item[foreign_column]) not in foreign_table_ids:
      item[foreign_column] = default_foreign_id

def __fix_all_foreign_columns(db_loader, csv_data_dict_list):
  for foreign_table, foreign_column in foreign_columns_to_fix:
    __fix_foreign_columns(foreign_table, foreign_column, db_loader, csv_data_dict_list)

def run_insertion():
  csv_data_dict_list = CsvUtils(excel_file, is_excel=True).get_data_dict_list()
  db_loader = MySqlUtils(db_endpoint, db_username, db_password, db_name)
  __fix_all_foreign_columns(db_loader, csv_data_dict_list)
  invalid_field_names, num_of_rows_inserted = db_loader.insert_data_dict_list_to_db(db_table_name, csv_data_dict_list, nullify_empty_values=False)

  if invalid_field_names:
    print('Invalid field names in csv/excel file:', ', '.join(invalid_field_names))
  print(f'{num_of_rows_inserted} records inserted successfully!')

if __name__ == '__main__':
  run_insertion()
