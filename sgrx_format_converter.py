import sys
import argparse
import re
from datetime import datetime

import format_converter_utils as utils
from common.build.data_field import DataField
from common.build.fixed_length_generator import FixedLengthGenerator
from common.send.aws_utils import DuplicateS3Key
from credentials import vendor_to_bucket_map
from send_file_to_ftp_site import run_send as send_file_to_ftp

# aws bucket
vendor = 'sgrx'
aws_s3_bucket_name = vendor_to_bucket_map[vendor]

def __validate_input():
  def validate_date_filter(val):
    if not re.match(r'^>?<?=?(>=)?(<=)?(!=)?(<>)?\d{4}-\d{2}-\d{2}$', val):
      raise argparse.ArgumentTypeError
    return val

  parser = argparse.ArgumentParser(description='Generates SGRX Eligibility Format and puts in S3 bucket.')
  parser.add_argument('--date-filter', dest='date_filter', required=False, type=validate_date_filter, help='The quoted date filter (format: YYYY-MM-DD) with >, <, =, >=, <=, !=, and <> operators like >=2019-03-01 (optional)')
  parser.add_argument('--output-definition-table-file', dest='output_definition_table_file', required=False, help='The name of data definition table csv file to be generated (optional)')
  parser.add_argument('--sync-all', dest='sync_all', action='store_true', help='If provided, the bucket will be synchronized to local path and all files will be overwritten (optional)')
  parser.add_argument('--send-to-ftp', dest='send_to_ftp', action='store_true', help='If provided, the created file will be sent to sftp server (optional)')
  try:
    args = parser.parse_args()
    return utils.get_date_filter_from_parameter(args.date_filter), args.output_definition_table_file, args.sync_all, args.send_to_ftp
  except:
    parser.print_help()
    sys.exit(0)

# get data from mysql db
def __get_db_data(date_filter=None):
  db_loader = utils.get_db_loader()
  query = r'''
    SELECT member.*,
      IF(UTC_DATE() BETWEEN member.eligibility_from AND member.eligibility_till, 'A', 'T') as status_code,
      CONCAT(benefit_plan.code, '') AS benefit_plan_code
    FROM member
    LEFT JOIN benefit_plan
      ON benefit_plan.id = member.benefit_plan_id
    WHERE
      (member.disenroll_date IS NULL OR member.disenroll_date = '0000-00-00 00:00:00')
      AND (member.terminate_date IS NULL OR member.terminate_date = '0000-00-00 00:00:00')
  '''
  if date_filter:
    query += ' AND member.enroll_date ' + date_filter
  db_dict_list = db_loader.fetch_all_from_query(query)

  for record in db_dict_list:
    postfix = float(str(record['postfix'] or '0').replace('.', ''))
    record['postfix'] = '1' if not postfix else '2' if postfix >= 1 and postfix <= 9 else '3'

    record['relation_type'] = {'other': '001', 'spouse': '002', 'child': '003'}.get((record['relation_type'] or '').lower(), '001')

  children_records = filter(lambda record: record['relation_type'] == '003', db_dict_list)
  parent_counter_map = {}
  parent_id_map = {}
  for child in reversed(sorted(children_records, key=lambda record: record['birthday'])):
    if child['relation_id']:
      if child['relation_id'] in parent_id_map:
        parent = parent_id_map[child['relation_id']]
      else:
        parent = list(filter(lambda row: child['relation_id'] and child['relation_id'] == row['id'], db_dict_list))
        parent = parent[0] if parent else None
        parent_id_map[child['relation_id']] = parent
      if parent:
        parent_id = parent['id']
        if parent_id in parent_counter_map:
          parent_counter_map[parent_id] = parent_counter_map[parent_id] + 1
        else:
          parent_counter_map[parent_id] = 3
        child['relation_type'] = str(parent_counter_map[parent_id]).zfill(3)

  return db_dict_list

# build converted data
def __build_data(db_dict_list):
  data_fields = [
    DataField(None, 'Customer ID', default_val='1082', val_len=10),
    DataField(None, 'Client ID', default_val='1000',val_len=15),
    DataField('benefit_plan_code', 'Group ID', {'8000': '8001BRONZE', '8001': '8001BRONZE', '8002': '8002SILVER', '8003': '8003GOLD'}, val_len=15),
    DataField('member_id', 'Member ID', val_len=18),
    DataField('relation_type', 'Person Code', val_len=3),
    DataField('status_code', 'Status', val_len=1),
    DataField('sex', 'Sex', {'Male': 'M', 'Female': 'F'}, val_len=1),
    DataField('postfix', 'Relationship Code', val_len=1),
    DataField('eligibility_from', 'Effective Date', utils.format_date, val_len=8),
    DataField(None, 'Eligibility Type', default_val='1', val_len=1),
    DataField('eligibility_till', 'Termination Date', utils.format_date, default_val='99991231', val_len=8),
    DataField('last_name', 'Last Name', val_len=15),
    DataField('first_name', 'First Name', val_len=12),
    DataField('address', 'Address Line 1', val_len=30),
    DataField(None, 'Address Line 2', val_len=25),
    DataField('city', 'City', val_len=20),
    DataField('state', 'State', val_len=2),
    DataField('zip_code', 'Zip Code', val_len=10),
    DataField(None, 'Plan ID Override', val_len=10),
    DataField('birthday', 'Birth Date', utils.format_date, val_len=8),
    DataField(None, 'Phone Number', val_len=10),
    DataField(None, 'Anniversary Date', utils.format_date, val_len=8),
    DataField(None, 'Patient ID Number', val_len=18),
    DataField(None, 'Primary Care Physician', val_len=10),
    DataField(None, 'Transaction Code', val_len=1),
    DataField(None, 'Alternate Member ID', val_len=18),
    DataField(None, 'Copay Override Flag', val_len=1),
    DataField(None, 'Copay Override (ID)', val_len=10),
    DataField(None, 'Primary Cov Insurance', val_len=15),
    DataField(None, 'Sub Grouping Code', val_len=10),
    DataField(None, 'Sub Grouping Effective Date', utils.format_date, val_len=8),
    DataField(None, 'Sub Grouping Termination Date', utils.format_date, val_len=8),
    DataField(None, 'Misc Grouping 1', val_len=10),
    DataField(None, 'Misc Grouping 2', val_len=10),
    DataField(None, 'Misc ID (PLAN CODE)', val_len=12),
    DataField(None, 'User Defined Code 1', val_len=2),
    DataField(None, 'User Defined Code 2', val_len=4),
    DataField(None, 'Card Request', val_len=1),
    DataField(None, 'Blank', val_len=7),
    DataField(None, 'Miscellaneous Data (RX PLAN ID)', val_len=30),
    DataField(None, 'Eligibility Validation ID', val_len=10),
    DataField(None, 'Pharmacy NABP', val_len=12),
    DataField(None, 'Coverage Strategy ID', val_len=10),
    DataField(None, 'Drug Coverage Strategy ID', val_len=10),
    DataField(None, 'Preferred Maintenance Drug Strategy ID', val_len=10),
    DataField(None, 'DUR Strategy ID', val_len=10),
    DataField(None, 'Pricing Strategy ID', val_len=10),
    DataField(None, 'Copay Strategy ID', val_len=10),
    DataField(None, 'Accum Benefit Strategy ID', val_len=10),
    DataField(None, 'Family ID', val_len=22),
    DataField(None, 'Expanded Patient ID', val_len=22),
    DataField(None, 'Country Code', val_len=4),
    DataField(None, 'Expanded Phone', val_len=15),
    DataField(None, 'FSA Indicator', val_len=1),
    DataField(None, 'FSA Member ID', val_len=18),
    DataField(None, 'FSA Product ID', val_len=15),
    DataField(None, 'Expanded First Name', val_len=25),
    DataField(None, 'Expanded Last Name', val_len=35),
    DataField(None, 'Expanded Address 1', val_len=55),
    DataField(None, 'Expanded Address 2', val_len=55),
    DataField(None, 'Expanded City', val_len=30),
    DataField(None, 'Expanded Zip Code', val_len=15),
    DataField(None, 'Injury Date', utils.format_date, val_len=8),
    DataField(None, 'Work Comp Ref No', val_len=30),
    DataField(None, 'Employer Name', val_len=55),
    DataField(None, 'Employer Addr 1', val_len=55),
    DataField(None, 'Employer Addr 2', val_len=55),
    DataField(None, 'Employer City', val_len=30),
    DataField(None, 'Employer State', val_len=2),
    DataField(None, 'Employer Country Code', val_len=4),
    DataField(None, 'Employer Zip Code', val_len=15),
    DataField(None, 'Employer Phone', val_len=15),
    DataField(None, 'Employer ID No', val_len=15),
    DataField(None, 'Claim Rep Name', val_len=55),
    DataField(None, 'Claim Rep Phone', val_len=15),
    DataField(None, 'Claim Rep Email', val_len=80),
    DataField(None, 'Alt Contact Name', val_len=55),
    DataField(None, 'Alt Contact Phone', val_len=15),
    DataField(None, 'State of Jurisdiction', val_len=2),
    DataField(None, 'Injury Description', val_len=120),
    DataField(None, 'Diagnosis ID', val_len=15),
    DataField(None, 'Diagnosis Eff Date', utils.format_date, val_len=8),
    DataField(None, 'Diagnosis Term Date', utils.format_date, val_len=8),
    DataField(None, 'LICS Indicator', val_len=1),
    DataField(None, 'LICS Level', val_len=1),
    DataField(None, 'Create Info Flag', val_len=1),
    DataField(None, 'User Defined Code', val_len=30),
    DataField(None, 'Spending Acct Plan Id', val_len=10),
    DataField(None, 'Internal Reject Override Id', val_len=10),
    DataField(None, 'Erisa Flag', val_len=1),
    DataField(None, 'Fund Type', val_len=1),
    DataField(None, 'Funding Type Code', val_len=8),
    DataField(None, 'Member Alternate Address Eff Date', utils.format_date, val_len=8),
    DataField(None, 'Member Alternate Address Term Date', utils.format_date, val_len=8),
    DataField(None, 'Member Alternate Address 1', val_len=55),
    DataField(None, 'Member Alternate Address 2', val_len=55),
    DataField(None, 'Member Alternate City', val_len=30),
    DataField(None, 'Member Alternate State', val_len=2),
    DataField(None, 'Member Alternate Zipcode', val_len=15),
    DataField(None, 'Written Language', val_len=3),
    DataField(None, 'Spoken Language', val_len=3),
    DataField(None, 'Race', val_len=5),
    DataField(None, 'Ethnicity', val_len=5),
    DataField(None, 'Institutional NGC Option', val_len=1),
    DataField(None, 'ESRD Option', val_len=1),
    DataField(None, 'Working Age Option', val_len=1),
    DataField(None, 'Active Retiree Option', val_len=1),
    DataField(None, 'Medicaid Option', val_len=1),
    DataField(None, 'Disability Option', val_len=1),
    DataField(None, 'Med D Group Type Option', val_len=1),
    DataField(None, 'Segment ID', val_len=3),
    DataField(None, 'CMS Contract Number', val_len=5),
    DataField(None, 'PBP ID Number', val_len=3),
    DataField(None, 'HICN RRB Option', val_len=1),
    DataField(None, 'HICN RRB Number', val_len=20),
    DataField(None, 'OCC Exception List ID ', val_len=10),
    DataField(None, 'Reject One Day Coverage', val_len=1),
    DataField(None, 'Member List ID', val_len=35),
    DataField(None, 'FIR Transaction Opt', val_len=1),
    DataField(None, 'Custom Reject Message', val_len=40),
    DataField(None, 'Expanded Fund Type', val_len=2),
    DataField(None, 'Accum Extract Type', val_len=2),
    DataField(None, 'Contract Type', val_len=4),
    DataField(None, 'Eligibility Lock ID', val_len=10),
    DataField(None, 'Incorrect Address Ind', val_len=1),
    DataField(None, 'Email', val_len=45),
    DataField(None, 'Limitation List ID', val_len=10)
  ]
  return FixedLengthGenerator(db_dict_list, data_fields)

# send converted data to local, s3 bucket, and sftp server
def __send_converted_data(output_generator, send_to_ftp):
  file_name = f'Elig_BENZER_{datetime.utcnow().strftime("%Y%m%d_%H%M%S")}.txt' # format: Elig_BENZER_CCYYMMDD_HHMMSS.txt
  file_name = f'outbox/{file_name}'
  utils.write_to_s3(output_generator, aws_s3_bucket_name, file_name)
  utils.write_to_local(output_generator, aws_s3_bucket_name, file_name)
  if send_to_ftp:
    print('Sending result to FTP site...')
    send_file_to_ftp(vendor, input_bucket_file_name=file_name)

def run_conversion(date_filter, output_definition_table_file, sync_all, send_to_ftp):
  db_dict_list = __get_db_data(date_filter)
  output_generator = __build_data(db_dict_list)
  __send_converted_data(output_generator, send_to_ftp)

  if sync_all:
    utils.sync_s3_with_local(aws_s3_bucket_name)

  if output_definition_table_file:
    output_generator.generate_data_definiton_table(output_definition_table_file)

if __name__ == '__main__':
  import warnings
  warnings.filterwarnings('ignore')

  date_filter, output_definition_table_file, sync_all, send_to_ftp = __validate_input()
  run_conversion(date_filter, output_definition_table_file, sync_all, send_to_ftp)