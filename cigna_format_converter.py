import sys
import argparse
import re
from datetime import datetime

import format_converter_utils as utils
from common.build.data_field import DataField
from common.build.fixed_length_generator import FixedLengthGenerator
from common.send.aws_utils import DuplicateS3Key
from credentials import vendor_to_bucket_map
from send_file_to_ftp_site import run_send as send_file_to_ftp

# aws bucket
vendor = 'cigna'
aws_s3_bucket_name = vendor_to_bucket_map[vendor]

def __validate_input():
  def validate_date_filter(val):
    if not re.match(r'^>?<?=?(>=)?(<=)?(!=)?(<>)?\d{4}-\d{2}-\d{2}$', val):
      raise argparse.ArgumentTypeError
    return val

  parser = argparse.ArgumentParser(description='Generates CIGNA Eligibility Format and puts in S3 bucket.')
  parser.add_argument('--date-filter', dest='date_filter', required=False, type=validate_date_filter, help='The quoted date filter (format: YYYY-MM-DD) with >, <, =, >=, <=, !=, and <> operators like >=2019-03-01 (optional)')
  parser.add_argument('--output-definition-table-file', dest='output_definition_table_file', required=False, help='The name of data definition table csv file to be generated (optional)')
  parser.add_argument('--sync-all', dest='sync_all', action='store_true', help='If provided, the bucket will be synchronized to local path and all files will be overwritten (optional)')
  parser.add_argument('--send-to-ftp', dest='send_to_ftp', action='store_true', help='If provided, the created file will be sent to sftp server (optional)')
  try:
    args = parser.parse_args()
    return utils.get_date_filter_from_parameter(args.date_filter), args.output_definition_table_file, args.sync_all, args.send_to_ftp
  except:
    parser.print_help()
    sys.exit(0)

# get data from mysql db
def __get_db_data(date_filter=None):
  db_loader = utils.get_db_loader()
  query = r'''
    SELECT member.*,
      member_external_keys.`value` AS ssn
    FROM member
    LEFT JOIN member_external_keys
      ON member_external_keys.member_id = member.id and member_external_keys.external_key_id = '3'
    WHERE
      (member.disenroll_date IS NULL OR member.disenroll_date = '0000-00-00 00:00:00')
      AND (member.terminate_date IS NULL OR member.terminate_date = '0000-00-00 00:00:00')
  '''
  if date_filter:
    query += ' AND member.enroll_date ' + date_filter
  db_dict_list = db_loader.fetch_all_from_query(query)

  # use pseudo ssn if member ssn is null
  pseudo_ssn_query = "SELECT ssn FROM pseudo_ssn WHERE utilized_date IS NULL ORDER BY id"
  pseudo_ssns = db_loader.fetch_all_from_query(pseudo_ssn_query)
  used_ssn_index = 0
  for record in db_dict_list:
    if record['postfix'] is not None:
      record['member_id'] = f"{record['member_id']}.{str(record['postfix']).zfill(2)}"
    if (record['relation_type'] or '').lower() not in ['child', 'spouse']:
      if not record['ssn'] and len(pseudo_ssns) > used_ssn_index:
        pseudo_ssn = pseudo_ssns[used_ssn_index]['ssn']
        record['ssn'] = pseudo_ssn
        db_loader.execute_query('UPDATE pseudo_ssn SET utilized_date=%s WHERE ssn=%s', [datetime.utcnow(), pseudo_ssn])
        used_ssn_index += 1
    else:
      record['dependent_ssn'] = record['ssn']
      if record['relation_id']:
        parent = list(filter(lambda row: record['relation_id'] == row['id'], db_dict_list))
        parent = parent[0] if parent else None
        record['ssn'] = parent['ssn'] if parent else ''
        if record['dependent_ssn'] == record['ssn']:
          record['dependent_ssn'] = ''

  return db_dict_list

# build converted data
def __build_data(db_dict_list):
  data_fields = [
    DataField(None, 'Group ID', default_val='8000', val_len=4),
    DataField('ssn', 'Employee SSN', lambda ssn: str(ssn).zfill(9) if ssn else '', val_len=9),
    DataField('member_id', 'Alternate Member ID', val_len=15),
    DataField(None, 'Dependent SSN', lambda ssn: str(ssn).zfill(9) if ssn else '', val_len=9), # 'dependent_ssn'
    DataField('last_name', 'Last Name', val_len=30),
    DataField('first_name', 'First Name', val_len=15),
    DataField('middle_name', 'Middle Initial', val_len=1),
    DataField('relation_type', 'Relationship to Employee', {'Spouse': '02', 'Child': '03'}, default_val='01', val_len=2),
    DataField('sex', 'Gender', {'Male': 'M', 'Female': 'F'}, val_len=1),
    DataField('birthday', 'Date of Birth', utils.format_date, val_len=8),
    DataField('address', 'Address Line 1', val_len=30),
    DataField(None, 'Address Line 2', val_len=30),
    DataField('city', 'City', val_len=20),
    DataField('state', 'State', val_len=2),
    DataField('zip_code', 'Zip Code', val_len=9),
    DataField('home_phone', 'Home Area Code and Phone Number', val_len=10),
    DataField('date_of_hire', 'Hire Date', utils.format_date, val_len=8),
    DataField(None, 'Division or Location', val_len=1),
    DataField(None, 'GWH-CIGNA Network Code', default_val='2004', val_len=4),
    DataField('eligibility_from', 'Effective date of network coverage', utils.format_date, val_len=8),
    DataField('eligibility_till', 'Termination date for network coverage', utils.format_date, val_len=8),
    DataField(None, 'PBM Plan ID', val_len=20),
    DataField(None, 'Effective date of PBM coverage', utils.format_date, val_len=8),
    DataField(None, 'Termination date for PBM coverage', utils.format_date, val_len=8),
    DataField(None, 'Dental Plan ID', val_len=20),
    DataField(None, 'Effective date of Dental coverage', utils.format_date, val_len=8),
    DataField(None, 'Termination date for Dental coverage', utils.format_date, val_len=8),
    DataField('state', 'State of Situs', val_len=2),
    DataField(None, 'Grandfathering Indicator', {'Not Grandfathered': 'N', 'Grandfathered': 'Y', 'Exempt': 'E'}, val_len=1, default_val='N'),
    DataField(None, 'ERISA Status', {'ERISA': 'E', 'Non-ERISA': 'N'}, val_len=1),
    DataField(None, 'Funding Type', {'Insured': 'FI', 'Self Funded': 'SF', 'Federally Funded': 'FE'}, val_len=2, default_val='SF'),
    DataField(None, 'Coverage Type', {'Group': 'G', 'Individual': 'I', 'Student': 'S'}, val_len=1, default_val='G'),
    DataField(None, 'Legal Entity', val_len=40),
    DataField(None, 'Native American Indicator', {'100-400% of FPL': '1', '250% of FPL': '2'}, val_len=1),
    DataField(None, 'Essential Health Benefit (EHB) State', val_len=2),
    DataField(None, 'Benefit Metal Level Code', {'bronze': 'B', 'silver': 'S', 'gold': 'G', 'platinum': 'P'}, val_len=1),
    DataField(None, 'Foreign Language Requirement Indicator', {'Yes': 'Y', 'No': 'N'}, val_len=1),
    DataField(None, 'Foreign Language as Primary Language', val_len=3)
  ]
  return FixedLengthGenerator(db_dict_list, data_fields)

# send converted data to local, s3 bucket, and sftp server
def __send_converted_data(output_generator, send_to_ftp):
  daily_counter = 1
  while True:
    file_name = f'N341000__n340002i.60544.P{datetime.utcnow().strftime("%Y%m%d")}-{daily_counter}.txt' # format: N341000__n340001i.60544.TCCYYMMDD-xxx.txt
    file_name = f'outbox/{file_name}'
    try:
      utils.write_to_s3(output_generator, aws_s3_bucket_name, file_name)
      break
    except DuplicateS3Key:
      daily_counter += 1
  utils.write_to_local(output_generator, aws_s3_bucket_name, file_name)
  if send_to_ftp:
    print('Sending result to FTP site...')
    send_file_to_ftp(vendor, input_bucket_file_name=file_name)

def run_conversion(date_filter, output_definition_table_file, sync_all, send_to_ftp):
  db_dict_list = __get_db_data(date_filter)
  output_generator = __build_data(db_dict_list)
  __send_converted_data(output_generator, send_to_ftp)

  if sync_all:
    utils.sync_s3_with_local(aws_s3_bucket_name)

  if output_definition_table_file:
    output_generator.generate_data_definiton_table(output_definition_table_file)

if __name__ == '__main__':
  import warnings
  warnings.filterwarnings('ignore')

  date_filter, output_definition_table_file, sync_all, send_to_ftp = __validate_input()
  run_conversion(date_filter, output_definition_table_file, sync_all, send_to_ftp)