import sys
import argparse
import re
from datetime import datetime

import format_converter_utils as utils
from common.build.data_field import DataField
from common.build.fixed_length_generator import FixedLengthGenerator
from common.build.csv_generator import CsvGenerator
from credentials import vendor_to_bucket_map
from send_file_to_ftp_site import run_send as send_file_to_ftp

# aws bucket
vendor = 'carevalet'
aws_s3_bucket_name = vendor_to_bucket_map[vendor]

def __validate_input():
  def validate_date_filter(val):
    if not re.match(r'^>?<?=?(>=)?(<=)?(!=)?(<>)?\d{4}-\d{2}-\d{2}$', val):
      raise argparse.ArgumentTypeError
    return val

  parser = argparse.ArgumentParser(description='Generates CARE VALET Eligibility Format and puts in S3 bucket.')
  parser.add_argument('--date-filter', dest='date_filter', required=False, type=validate_date_filter, help='The quoted date filter (format: YYYY-MM-DD) with >, <, =, >=, <=, !=, and <> operators like >=2019-03-01 (optional)')
  parser.add_argument('--pipe-delimited', dest='pipe_delimited', action='store_true', help='If provided, the output format will be pipe-delimited instead of fixed-size')
  parser.add_argument('--output-definition-table-file', dest='output_definition_table_file', required=False, help='The name of data definition table csv file to be generated (optional)')
  parser.add_argument('--sync-all', dest='sync_all', action='store_true', help='If provided, the bucket will be synchronized to local path and all files will be overwritten (optional)')
  parser.add_argument('--send-to-ftp', dest='send_to_ftp', action='store_true', help='If provided, the created file will be sent to sftp server (optional)')
  try:
    args = parser.parse_args()
    return utils.get_date_filter_from_parameter(args.date_filter), args.pipe_delimited, args.output_definition_table_file, args.sync_all, args.send_to_ftp
  except:
    parser.print_help()
    sys.exit(0)

# get data from mysql db
def __get_db_data(date_filter=None):
  db_loader = utils.get_db_loader()
  query = r'''
    SELECT member.*,
      CONCAT(benefit_plan.code, '') AS subgroup_identifier,
      member_external_keys.`value` AS ssn
    FROM member
    LEFT JOIN benefit_plan
      ON benefit_plan.id = member.benefit_plan_id
    LEFT JOIN member_external_keys
      ON member_external_keys.member_id = member.id and member_external_keys.external_key_id = '3'
    WHERE
      (member.disenroll_date IS NULL OR member.disenroll_date = '0000-00-00 00:00:00')
      AND (member.terminate_date IS NULL OR member.terminate_date = '0000-00-00 00:00:00')
  '''
  if date_filter:
    query += ' AND member.enroll_date ' + date_filter
  db_dict_list = db_loader.fetch_all_from_query(query)

  for record in db_dict_list:
    record['phone_number'] = ''.join(re.findall(r'\d+', record['phone'] or record['home_phone'] or record['work_phone'] or ''))
    record['employee_status'] = 'AC' if record['eligibility_till'] >= datetime.utcnow().date() else 'TE'
    if (record['relation_type'] or '').lower() in ['child', 'spouse']:
      if record['postfix']:
        record['member_id'] = f"{record['member_id']}.{str(record['postfix']).zfill(2)}"
      record['email'] = ''
      record['ssn'] = ''
      parent = list(filter(lambda row: record['relation_id'] and record['relation_id'] == row['id'], db_dict_list))
      parent = parent[0] if parent else None
      record['insured_last_name'] = parent['last_name'] if parent else ''
      record['insured_first_name'] = parent['first_name'] if parent else ''
      record['insured_middle_name'] = parent['middle_name'] if parent else ''
      record['address'] = parent['address'] if parent else ''
      record['city'] = parent['city'] if parent else ''
      record['state'] = parent['state'] if parent else ''
      record['zip_code'] = parent['zip_code'] if parent else ''
    else:
      record['ssn'] = str(record['ssn']).zfill(9) if record['ssn'] else ''
      record['insured_last_name'] = record['last_name']
      record['insured_first_name'] = record['first_name']
      record['insured_middle_name'] = record['middle_name']

  return db_dict_list

# build converted data
def __build_data(db_dict_list, pipe_delimited=False):
  data_fields = [
    DataField('relation_type', 'Relationship Code', {'Spouse': '01', 'Child': '19'}, default_val='18', val_len=2),
    DataField(None, 'Benefit Status', default_val='A', val_len=1),
    DataField('member_id', 'Member Id', val_len=30),
    DataField(None, 'Person Code', val_len=2),
    DataField(None, 'Group Policy Number', default_val='8000', val_len=30),
    DataField('insured_last_name', 'Insured Last', val_len=35),
    DataField('insured_first_name', 'Insured First', val_len=25),
    DataField('insured_middle_name', 'Insured Middle', val_len=25),
    DataField('last_name', 'Member Last', val_len=35),
    DataField('first_name', 'Member First', val_len=25),
    DataField('middle_name', 'Member Middle', val_len=25),
    DataField('ssn', 'Member SSN', val_len=80),
    DataField('email', 'Member Email', val_len=150),
    DataField('phone_number', 'Member Phone', val_len=80),
    DataField('address', 'Member Address1', val_len=55),
    DataField(None, 'Member Address2', val_len=55),
    DataField('city', 'Member City', val_len=30),
    DataField('state', 'Member State', val_len=2),
    DataField('zip_code', 'Member Zip Code', val_len=15),
    DataField('birthday', 'Date of Birth', utils.format_date, val_len=8),
    DataField('sex', 'Member Gender', {'Male': 'M', 'Female': 'F'}, default_val='U', val_len=1),
    DataField('eligibility_from', 'Member Begin Date', utils.format_date, val_len=8),
    DataField('eligibility_till', 'Member Term Date', utils.format_date, val_len=8),
    DataField(None, 'Member Copay', default_val='000000000', val_len=9),
    DataField(None, 'Member Deductible', default_val='000000000', val_len=9),
    DataField(None, 'Cobra Begin Date', utils.format_date, val_len=8),
    DataField(None, 'Cobra End Date', utils.format_date, val_len=8),
    DataField('employee_status', 'Employee Status', val_len=2),
    DataField(None, 'Coverage Level', {'Dental': 'DEN', 'Health': 'HLT', 'Vision': 'VIS'}, default_val='HLT', val_len=3),
    DataField('subgroup_identifier', 'Sub Group Identifier', {'8001': 'BRONZE', '8002': 'SILVER', '8003': 'GOLD'}, val_len=10),
    DataField(None, 'MemberMaxOutOfPocket', default_val='000000000', val_len=9),
    DataField(None, 'MemberCurrentOotOfPocket', default_val='000000000', val_len=9)
  ]
  if not pipe_delimited:
    return FixedLengthGenerator(db_dict_list, data_fields)
  else:
    return CsvGenerator(db_dict_list, data_fields, delimiter='|')

# send converted data to local, s3 bucket, and sftp server
def __send_converted_data(output_generator, send_to_ftp):
  file_name = f'Elig_CAREVALET_{datetime.utcnow().strftime("%Y%m%d_%H%M%S")}.txt' # format: Elig_CAREVALET_CCYYMMDD_hhmmss.txt
  file_name = f'outbox/{file_name}'
  utils.write_to_s3(output_generator, aws_s3_bucket_name, file_name)
  utils.write_to_local(output_generator, aws_s3_bucket_name, file_name)
  if send_to_ftp:
    print('Sending result to FTP site...')
    send_file_to_ftp(vendor, input_bucket_file_name=file_name)

def run_conversion(date_filter, pipe_delimited, output_definition_table_file, sync_all, send_to_ftp):
  db_dict_list = __get_db_data(date_filter)
  output_generator = __build_data(db_dict_list, pipe_delimited)
  __send_converted_data(output_generator, send_to_ftp)

  if sync_all:
    utils.sync_s3_with_local(aws_s3_bucket_name)

  if output_definition_table_file:
    output_generator.generate_data_definiton_table(output_definition_table_file)

if __name__ == '__main__':
  import warnings
  warnings.filterwarnings('ignore')

  date_filter, pipe_delimited, output_definition_table_file, sync_all, send_to_ftp = __validate_input()
  run_conversion(date_filter, pipe_delimited, output_definition_table_file, sync_all, send_to_ftp)