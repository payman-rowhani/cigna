import sys
import os
import traceback
import argparse

from credentials import (vendor_to_bucket_map,
                         get_aws_utils,
                         get_ftp_credentials)

def __validate_input():
  parser = argparse.ArgumentParser(description='Upload files from s3 buckets to ftp sites.')
  parser.add_argument('--vendor', dest='vendor', required=True, choices=vendor_to_bucket_map.keys(), help=f'Vendor name')
  parser.add_argument('--send-latest', dest='send_latest', action='store_true', help='If provided, the latest file of the vendor is uploaded to ftp site (Optional; should not be passed with --file-name)')
  parser.add_argument('--file-name', dest='file_name', required=False, help='The file name to upload to ftp site (Optional; should not be passed with --send-latest)')

  try:
    args = parser.parse_args()
    return args.vendor, args.send_latest, args.file_name
  except:
    parser.print_help()
    sys.exit(0)

def __get_file_to_upload(vendor, latest_file_on_bucket=False, bucket_file_name=None):
  if bucket_file_name and latest_file_on_bucket:
    print(f'Either file name or latest file should be passed, not both!')
    sys.exit(0)
  elif not bucket_file_name and not latest_file_on_bucket:
    print(f'At least one of file name or latest file should be passed!')
    sys.exit(0)

  bucket_name = vendor_to_bucket_map[vendor]
  aws_utils = get_aws_utils()

  if latest_file_on_bucket:
    bucket_file_name = aws_utils.get_s3_file_list(bucket_name, prefix='outbox/')[0]
  elif not bucket_file_name.startswith('outbox/'):
    bucket_file_name = 'outbox/' + bucket_file_name

  downloaded_file_name = ''
  try:
    downloaded_file_name = aws_utils.download_s3_file(bucket_name, bucket_file_name)
    print(f'Got the file from bucket "{bucket_name}" successfully:', bucket_file_name)
  except:
    print(f'Could not download file "{bucket_file_name}" from bucket "{bucket_name}"')
    sys.exit(0)

  return os.path.basename(bucket_file_name), downloaded_file_name

def __upload_file_to_ftp(ftp_credentials, ftp_utils, ftp_file_name, file_name_to_upload):
  try:
    ftp_file_name = f'{ftp_credentials["directory"]}/{ftp_file_name}'
    ftp_utils.upload_file(file_name_to_upload, ftp_file_name)
    print(f'Uploaded the file to ftp site "{ftp_credentials["address"]}" successfully:', ftp_file_name)
  except Exception as e:
    print(f'Could not upload the file to the ftp server "{ftp_credentials["address"]}"!')
    print('Error details:', e)
    traceback.print_exc()
    sys.exit(0)

def run_send(vendor, latest_file_on_bucket=False, input_bucket_file_name=None):
  bucket_file_name, downloaded_file_name = __get_file_to_upload(vendor, latest_file_on_bucket, input_bucket_file_name)
  ftp_credentials, ftp_utils = get_ftp_credentials(vendor)
  __upload_file_to_ftp(ftp_credentials, ftp_utils, bucket_file_name, downloaded_file_name)
  os.remove(downloaded_file_name)

if __name__ == '__main__':
  import warnings
  warnings.filterwarnings('ignore')

  vendor, latest_file_on_bucket, input_bucket_file_name = __validate_input()
  run_send(vendor, latest_file_on_bucket, input_bucket_file_name)