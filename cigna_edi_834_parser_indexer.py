import sys
import argparse

from common.send.aws_utils import AwsUtils
from common.build.edi_parser.util import get_834_5010_facade
from credentials import (test_aws_region_name,
                         test_aws_access_key_id,
                         test_aws_secret_access_key)

default_es_host = 'https://search-cigna-members-fnkiri4c554rd4ett4a5bovdpq.us-east-1.es.amazonaws.com'

def __get_cigna_members(input_file, show_output=True):
  if show_output:
    print(f'Parsing EDI 834-5010 {input_file}...')

  def get_enum_desc(field):
    return field[-1] if field else None

  x12_facade = get_834_5010_facade(input_file)
  members = []
  for subscriber in x12_facade.subscribers:
    member = {
      'sponsor_name': x12_facade.sponsor.name,
      'sponsor_federal_taxpayer_id': x12_facade.sponsor.federal_taxpayer_id,
      'payer_name': x12_facade.payer.name,
      'payer_federal_taxpayer_id': x12_facade.payer.federal_taxpayer_id,
      'response_code': get_enum_desc(subscriber.member_level.response_code),
      'relationship_code': get_enum_desc(subscriber.member_level.relationship_code),
      'benefit_status_code': get_enum_desc(subscriber.member_level.benefit_status_code),
      'medicare_plan_code': get_enum_desc(subscriber.member_level.medicare_plan_code),
      'subscriber_ssn': subscriber.member_level.subscriber_ssn,
      'last_name': subscriber.member.last_name if subscriber.member else None,
      'first_name': subscriber.member.first_name if subscriber.member else None,
      'middle_name': subscriber.member.middle_name if subscriber.member else None,
      'member_ssn': subscriber.member.member_ssn if subscriber.member else None,
      'address_line1': subscriber.member.address_line1 if subscriber.member else None,
      'address_line2': subscriber.member.address_line2 if subscriber.member else None,
      'city': subscriber.member.city if subscriber.member else None,
      'state': subscriber.member.state if subscriber.member else None,
      'zip_code': subscriber.member.zip_code if subscriber.member else None,
      'birth_date': subscriber.member.birth_date.isoformat() if subscriber.member else None,
      'gender': get_enum_desc(subscriber.member.gender) if subscriber.member else None,
      'marital_status': get_enum_desc(subscriber.member.marital_status) if subscriber.member else None,
      'insurance_line_code': get_enum_desc(subscriber.coverage.insurance_line_code) if subscriber.coverage else None,
      'coverage_description': subscriber.coverage.coverage_description if subscriber.coverage else None,
      'coverage_level_code': subscriber.coverage.coverage_level_code if subscriber.coverage else None,
      'benefit_begin_date': subscriber.coverage.benefit_begin_date.isoformat() if subscriber.coverage else None,
      'plan_code': subscriber.coverage.plan_code if subscriber.coverage else None,
      'plan_account_number': subscriber.coverage.plan_account_number if subscriber.coverage else None,
      'plan_branch': subscriber.coverage.plan_branch if subscriber.coverage else None,
      'plan_benefit_options': subscriber.coverage.plan_benefit_options if subscriber.coverage else None,
      'plan_network': subscriber.coverage.plan_network if subscriber.coverage else None,
      'pcp_code': subscriber.provider.pcp_code if subscriber.provider else None,
    }
    members.append(member)

  if show_output:
    print(f'Parsed EDI 834-5010 input file and extracted {len(members)} entries successfully.')
  return members

def run_parse_and_index_edi_834(input_file, bulk_size, es_host):
  cigna_members = __get_cigna_members(input_file)
  aws_utils = AwsUtils(test_aws_region_name, test_aws_access_key_id, test_aws_secret_access_key)
  aws_utils.create_bulk_es_index(es_host, 'members', 'member', cigna_members, bulk_size=bulk_size)

def __validate_input():
  parser = argparse.ArgumentParser(description='Parses X12 834-5010 EDI file and sends the result to AWS ElasticSearch Service.')
  parser.add_argument('input_file', nargs=1, help='Path to EDI 834 file')
  parser.add_argument('--bulk-size', dest='bulk_size', type=int, required=False, help=f'Number of items to create index for in each bulk (defaults to 100; 0 means all at once)', default=100)
  parser.add_argument('--es-host', dest='es_host', required=False, help=f'ES host', default=default_es_host)
  try:
    args = parser.parse_args()
    return args.input_file[0], args.bulk_size, args.es_host
  except:
    parser.print_help()
    sys.exit(0)

if __name__ == '__main__':
  import warnings
  warnings.filterwarnings('ignore')

  input_file, bulk_size, es_host = __validate_input()
  run_parse_and_index_edi_834(input_file, bulk_size, es_host)
