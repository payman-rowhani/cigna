import sys
import argparse
import traceback

from credentials import db_alias_to_credentials_map, get_db_loader

def __validate_input():
  parser = argparse.ArgumentParser(usage='%(prog)s --source-db RDS --source-table member --target-db PROD --target-table member', description='Clone a table from a source db to a table on the target db.')
  parser.add_argument('--source-db', dest='source_db_alias', required=True, choices=db_alias_to_credentials_map.keys(), help=f'Source db alias name')
  parser.add_argument('--source-table', dest='source_table_name', required=True, help='Source db table name')
  parser.add_argument('--target-db', dest='target_db_alias', required=True, choices=db_alias_to_credentials_map.keys(), help=f'Target db alias name')
  parser.add_argument('--target-table', dest='target_table_name', required=False, help='Target db table name (if not passed, source db table name will be used)')

  try:
    args = parser.parse_args()
    return args.source_db_alias, args.source_table_name, args.target_db_alias, (args.target_table_name or args.source_table_name)
  except:
    parser.print_help()
    sys.exit(0)

def __get_source_table_data(source_db_alias, source_table_name):
  db_loader = get_db_loader(source_db_alias, use_single_connection=False)
  try:
    source_table_data = db_loader.fetch_all_from_table(source_table_name)
    return source_table_data
  except Exception as e:
    print(f'Error: Invalid source table \'{source_table_name}\' passed!')
    print('Error details:', e)
    traceback.print_exc()
    sys.exit(0)

def __copy_to_target_table(target_db_alias, target_table_name, source_table_data):
  db_loader = get_db_loader(target_db_alias)
  try:
    db_loader.fetch_all_from_query(f'SELECT 1 from {target_table_name}')
  except Exception as e:
    print(f'Error: Invalid target table \'{target_table_name}\' passed!')
    print('Error details:', e)
    traceback.print_exc()
    sys.exit(0)

  invalid_field_names, num_of_rows_inserted = db_loader.insert_data_dict_list_to_db(target_table_name, source_table_data, nullify_empty_values=False)
  db_loader.close_db_connection()
  return invalid_field_names, num_of_rows_inserted

def run_insertion(source_db_alias, source_table_name, target_db_alias, target_table_name):
  source_table_data = __get_source_table_data(source_db_alias, source_table_name)
  invalid_field_names, num_of_rows_inserted = __copy_to_target_table(target_db_alias, target_table_name, source_table_data)

  if invalid_field_names:
    print('Warning: Invalid field names in source table:', ', '.join(invalid_field_names))
  print(f'{num_of_rows_inserted} records inserted successfully!')

if __name__ == '__main__':
  source_db_alias, source_table_name, target_db_alias, target_table_name = __validate_input()
  run_insertion(source_db_alias, source_table_name, target_db_alias, target_table_name)