from send_decrypted_file_to_s3 import run_send

if __name__ == '__main__':
  import warnings
  warnings.filterwarnings('ignore')

  run_send('uzio',
            should_decrypt=False,
            extract_dirs_from_ftp=True,
            is_claims_credential=False,
            sftp_port=9022)